-- Dummy schema that represents the util lib.
drop schema IF EXISTS UTL_MATCH CASCADE;
create schema UTL_MATCH;

-- Dummy fuction to test Contact Repository as it uses a Native Oracle function
CREATE function UTL_MATCH.JARO_WINKLER_SIMILARITY (in s1 VARCHAR(4000), in s2 VARCHAR(4000)) 
  	RETURNS INT
 LANGUAGE JAVA DETERMINISTIC NO SQL
   EXTERNAL NAME 'CLASSPATH:com.example.connect.database.util.StringUtils.getJaroWinklerDistance';

