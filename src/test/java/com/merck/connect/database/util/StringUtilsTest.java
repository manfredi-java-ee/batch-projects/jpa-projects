package com.example.connect.database.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.example.connect.database.util.StringUtils;

public class StringUtilsTest {
	@Test
	public void testGetJaroWinklerDistance() {
		assertEquals(0, StringUtils.getJaroWinklerDistance(null, null));
		assertEquals(0, StringUtils.getJaroWinklerDistance("", ""));
		assertEquals(88, StringUtils.getJaroWinklerDistance("Jose David", "Jose"));
		assertEquals(43, StringUtils.getJaroWinklerDistance("Jose David","David"));
		assertEquals(92, StringUtils.getJaroWinklerDistance("Jose David", "Jose D"));
		assertEquals(20, StringUtils.getJaroWinklerDistance("Jose David", "John Dante"));
	}

}
