package com.example.connect.database.model.activitytask;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import com.example.connect.database.model.activitytask.ActivityTaskRule;

public class ActivityTaskRuleTest {

	@Test
	public void applyRuleWithFalseFieldStringLiteral() {
		ActivityTaskRule rule = new ActivityTaskRule();
		rule.setField("false");
		
		assertThat(rule.applyRule(), is(Boolean.FALSE));
	}

	@Test
	public void applyRuleWithEmptyField() {
		ActivityTaskRule rule = new ActivityTaskRule();
		rule.setField("");
		
		assertThat(rule.applyRule(), is(Boolean.FALSE));
	}

	@Test
	public void applyRuleWithTrueFieldStringLiteral() {
		ActivityTaskRule rule = new ActivityTaskRule();
		rule.setField("TRUE");
		
		assertThat(rule.applyRule(), is(Boolean.TRUE));
	}

	@Test
	public void applyRuleWithNullField() {
		ActivityTaskRule rule = new ActivityTaskRule();
		rule.setField(null);
		
		assertThat(rule.applyRule(), is(Boolean.FALSE));
	}

}
