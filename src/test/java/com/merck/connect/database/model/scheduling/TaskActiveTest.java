package com.example.connect.database.model.scheduling;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TaskActiveTest {

	@Test
	public void testName() {
		assertEquals("Y", TaskActive.Y.name());
		assertEquals("N", TaskActive.N.name());
	}
	
	@Test
	public void testTaskActiveLength() {
		assertEquals("There should be 2 task active values defined inside the enumeration.", 
				2, TaskActive.values().length);
	}
	
	@Test
	public void testToString() {
		assertEquals("Y", TaskActive.Y.toString());
		assertEquals("N", TaskActive.N.toString());
	}

}
