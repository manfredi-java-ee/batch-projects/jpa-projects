package com.example.connect.database.model.scheduling;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TaskStatusTest {

	@Test
	public void testName() {
		assertEquals("COMPLETED", TaskStatus.COMPLETED.name());
		assertEquals("ERROR", TaskStatus.ERROR.name());
		assertEquals("IN_PROGRESS", TaskStatus.IN_PROGRESS.name());
		assertEquals("RESTART", TaskStatus.RESTART.name());
	}
	
	@Test
	public void testTaskStatusLength() {
		assertEquals("There should be 4 task status values defined inside the enumeration.", 
				5, TaskStatus.values().length);
	}
	
	@Test
	public void testToString() {
		assertEquals("COMPLETED", TaskStatus.COMPLETED.toString());
		assertEquals("ERROR", TaskStatus.ERROR.toString());
		assertEquals("IN_PROGRESS", TaskStatus.IN_PROGRESS.toString());
		assertEquals("RESTART", TaskStatus.RESTART.toString());
	}

}
