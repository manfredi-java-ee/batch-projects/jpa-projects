package com.example.connect.database.repository.alert;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service.xml" })
public class AlertRepositoryTest {

	@Autowired
	@Qualifier(value = "alertRepositoryJDBCForLA")
	private AlertRepository alertRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRunAlertForLA() {
	 
		boolean result = alertRepositoryJDBCForLA.runAlert("SODADM", "T002A", "1", true);
	 
		assertEquals(false, result);
 
	}

 
	 

}
