package com.example.connect.database.repository.contact.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class ContactAccountRepositoryJDBCTest {
	@Autowired
	@Qualifier(value = "contactAccountRepositoryJDBCForLA")
	private ContactAccountRepository contactAccountRepository;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testNotFoundAccountAssociatedToContactAndBeInPanel() {
		
		final String contactId = "BR-XXXXX";   
		final String userId    = "AJIA-1CIOBG";
  	
		final List<Map<String, Object>> contactList = contactAccountRepository.getAccountAssociatedToContactAndBeInPanel(contactId, userId);
		
		assertTrue(contactList.isEmpty());
	}

 	@Test
	public void testNotFoundAccountAssociatedToOtherContact() {
	
 		
 		final String contactId = "BR-XXXXX";   
		final String userId    = "AJIA-1CIOBG";
 		final String accountId = "BR-XXXXX";   

 		
		final List<Map<String, Object>> contactList = contactAccountRepository.getAccountAssociatedToOtherContact(contactId, accountId, userId);
		
		assertTrue(contactList.isEmpty());
		
		
 	}

  	@Test
	public void testFoundAccountAssociatedToContactAndBeInPanel() {
		
		final String contactId = "AGEA-4RWZ82";   
		final String userId    = "AGEA-5DD5ZJ";
  	
		final List<Map<String, Object>> contactList = contactAccountRepository.getAccountAssociatedToContactAndBeInPanel(contactId, userId);
		
		Map<String,Object> map = contactList.get(0);
		
	 	assertEquals("AJIA-53JA2I",map.get("ACCOUNTID"));
		assertEquals("AGEA-4RWZ82",map.get("CONTACTID"));
	}

 	@Test
	public void testFoundAccountAssociatedToOtherContact() {
	
 		
 		final String contactId = "AGEA-4RZB6K";   
		final String userId    = "AGEA-5DD5ZJ";
 		final String accountId = "AGEA-52KUN6";   
 	
		final List<Map<String, Object>> contactList = contactAccountRepository.getAccountAssociatedToOtherContact(contactId, accountId, userId);
		
		Map<String,Object> map = contactList.get(0);

	 	assertEquals("AGEA-52KUN6",map.get("ACCOUNTID"));
		assertEquals("AGEA-4RVZAM",map.get("CONTACTID"));
		
  	}
	 
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
}
