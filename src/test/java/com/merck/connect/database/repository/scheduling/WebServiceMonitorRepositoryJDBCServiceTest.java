package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class WebServiceMonitorRepositoryJDBCServiceTest {

	@Autowired
	@Qualifier(value = "webServiceMonitorRepositoryServiceForLA")
	private WebServiceMonitorRepositoryService webServiceMonitorRepositoryServiceForLA;
	
	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testFindByWsName() {
		assertNotNull(webServiceMonitorRepositoryServiceForLA.findByWsName("WS-PROCESSID-6"));
	}

}
