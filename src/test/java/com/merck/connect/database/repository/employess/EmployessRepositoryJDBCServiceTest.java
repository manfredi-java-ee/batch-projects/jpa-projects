package com.example.connect.database.repository.employess;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class EmployessRepositoryJDBCServiceTest {

	@Autowired
	@Qualifier(value = "employessRepositoryJDBCForLA")
	private  EmployessRepository employessRepositoryForLA;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}	
	
	@Test
	public void testFindByRowidForLA() {
		
		List<Map<String, Object>>  listUser = employessRepositoryForLA.findByRowId("AJIA-EIXZIK");
			
		assertEquals(1, listUser.size());		
	
	
	}
	
	@Test
	public void testGetValueByRowiForLA()  {
		 
		List<Map<String, Object>> listUser = employessRepositoryForLA.findByRowId("AJIA-AY4Y3I");
	 	
		Map<String, Object> map = listUser.get(0);
		String businessunit = (String)map.get("BUSINESSUNIT");
		
				
//		for(Map user : listUser) {
//		     String brand = (String)user.get("brand");
//		     String name = (String)user.get("name");
// 
//		}
		
		
		assertEquals("KAM", businessunit);		
	
	
	}
	

	@Test
	public void testFindByExternalIdForLA() {
	 
		List<Map<String, Object>>  listUser = employessRepositoryForLA.findByExternalId("53078681");
		
		assertEquals(1, listUser.size());		
	}

 
	
	@Test
	public void testGetValueByExternalIdForLA()  {
		 
		List<Map<String, Object>> listUser = employessRepositoryForLA.findByExternalId("53078681");
	 	
		Map<String, Object> map = listUser.get(0);
		String businessunit = (String)map.get("BUSINESSUNIT");
		
 
		
		assertEquals("PRIMARY CARE", businessunit);		
	
	
	}
	
	
	
	
}
