package com.example.connect.database.repository.contact;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerBR")
@Rollback
public class ContactRepositoryJDBCForBRTest {
	@Autowired
	@Qualifier(value = "contactRepositoryJDBCForBR")
	private ContactRepository contactRepositoryForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}


	@Test
	public void testGetContactQuery() {
		assertEquals("findContMedIdQuery", contactRepositoryForBR.getContactQuery());
	}
	
	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNameFullMatched() {
		final String firstName = "", lastName = "CAMILA DELFINO", medicalId = "0119173", state = "SP", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(1, contactList.size());
		assertContact(Iterables.getOnlyElement(contactList));
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNamePartialMatched() {
		final String firstName = "", lastName = "CAMILA", medicalId = "0119173", state = "SP", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdNotFoundAndStateAndLastNameFullMatched() {
		final String firstName = "", lastName = "CAMILA DELFINO", medicalId = "0999188", state = "SP", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndLastNameFullMatchedAndStateNotFound() {
		final String firstName = "", lastName = "CAMILA DELFINO", medicalId = "0119173", state = "ZZ", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNameFullMatchedWithSpecialtyOrderDistinctOfOne() {
		final String firstName = "", lastName = "RODRIGO BRUNO BIAGIONI", medicalId = "0105547", state = "AL", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNameFullMatchedWithExtIdNull() {
		final String firstName = "", lastName = "DAIANA ANDRADE", medicalId = "0105566", state = "PE", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForBR.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}


	private void assertContact(final Map<String, Object> contactMap) {
		assertEquals("Brazil", contactMap.get("COUNTRY"));
		assertEquals("AJIA-3QTBZX", contactMap.get("CONTACTID"));
		assertEquals(".", contactMap.get("CONTACTFIRSTNAME"));
		assertEquals("CAMILA DELFINO", contactMap.get("CONTACTLASTNAME"));
		assertEquals("camiladelf@HOTMAIL.com", contactMap.get("CONTACTEMAIL"));
		assertEquals("Physician", contactMap.get("CONTACTTYPE"));
		assertEquals("0119173", contactMap.get("LOCALREF"));
		assertEquals("BR-0000557248", contactMap.get("EXTID"));
		assertEquals("Active", contactMap.get("STATUS"));
		assertEquals("AJIA-3RNGRJ", contactMap.get("ACCOUNTID"));
		assertEquals("SP", contactMap.get("STATE"));
	}

}
