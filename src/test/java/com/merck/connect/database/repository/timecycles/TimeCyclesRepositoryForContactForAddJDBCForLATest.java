package com.example.connect.database.repository.timecycles;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class TimeCyclesRepositoryForContactForAddJDBCForLATest {

	@Autowired
	@Qualifier(value = "timeCyclesRepositoryForContactForAddJDBCForLA")
	private TimeCyclesRepository timeCyclesRepositoryForContactForAddJDBCForLA;


	@BeforeClass
	public static void setSystemProps() {
		System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetWindows() throws Exception {
		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddJDBCForLA.getWindows();
		assertEquals(4, map.size());

		final Map<String, Object> result = map.get(0);
		final Date sf = new SimpleDateFormat("yyyyMMdd").parse("20400229");
		assertEquals(sf, result.get("CRE_APP_DT"));
		assertEquals("NA", result.get("BUSINESS_UNIT"));
	}


}
