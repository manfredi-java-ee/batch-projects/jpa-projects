package com.example.connect.database.repository.account;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-mx-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerMX")
@Rollback
public class AccountRepositoryJDBCForMXTest {

	@Autowired
	@Qualifier(value = "accountRepositoryJDBCForMX")
	private AccountRepository accountRepositoryForMX;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetAccountQuery() {
		assertEquals("findAccNameQuery", accountRepositoryForMX.getAccountQuery());
	}

	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String accountName = "", colony = "", cnpj = "";
		final List<Map<String, Object>> contactList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyMatched() {
		final String accountName = "MED TEC 100-TORRE MED TEC 100-CONS 408", colony = "EL CARRIZAL", cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(1, accountList.size());
		assertAccount(Iterables.getOnlyElement(accountList), "AJIA-410N4T", "MED TEC 100-TORRE MED TEC 100-CONS 408",
				"Pvt Hospital", "AJIA-265267", "MX-0000563133", "Active", "EL CARRIZAL");
	}

	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyNotFound() {
		final String accountName = "MED TEC 100-TORRE MED", colony = "EL CARRIZAL", cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}
	
	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyEmptyNotFound() {
		final String accountName = "F. ERICKA-GENERAL PUEBLITA NTE 18", colony = "", cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}

	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyNullMatched() {
		final String accountName = "F. ERICKA-GENERAL PUEBLITA NTE 18", colony = null, cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(1, accountList.size());
		assertAccount(Iterables.getOnlyElement(accountList), "AJIA-4113HB", "F. ERICKA-GENERAL PUEBLITA NTE 18",
				"Pharmacy", "AJIA-2YOZKM", "MX-0000518835", "Active", null);
	}

	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyWithExtIdWithoutMXPrefix() {
		final String accountName = "TORRE MEDICA PROVIDENCIA@2", colony = "LOMAS DE PROVIDENCIA", cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}

	public void testFindByCustomFieldsByAccountNameAndColonyWithExtIdNull() {
		final String accountName = "MED TEC 100-TORRE MED TEC 100-CONS 904", colony = "EL CARRIZAL", cnpj = "";
		final List<Map<String, Object>> accountList = accountRepositoryForMX.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}


	private void assertAccount(final Map<String, Object> accountMap, final String accountIdExpected, 
			final String accountNameExpected, final String accountTypeExpected, final String userIdExpected, 
			final String extIdExpected, final String statusExpected, final String colonyExpected) {
		assertEquals(accountIdExpected, accountMap.get("ACCOUNTID"));
		assertEquals(accountNameExpected, accountMap.get("ACCOUNTNAME"));
		assertEquals(accountTypeExpected, accountMap.get("ACCOUNTTYPE"));
		assertEquals(userIdExpected, accountMap.get("USERID"));
		assertEquals(extIdExpected, accountMap.get("EXTID"));
		assertEquals(statusExpected, accountMap.get("STATUS"));
		assertEquals(colonyExpected, accountMap.get("COLONY"));
	}

}
