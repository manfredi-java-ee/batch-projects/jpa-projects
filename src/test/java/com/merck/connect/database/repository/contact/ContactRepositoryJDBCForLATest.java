package com.example.connect.database.repository.contact;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-with-functions-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class ContactRepositoryJDBCForLATest {

	@Autowired
	@Qualifier(value = "contactRepositoryJDBCForLA")
	private ContactRepository contactRepositoryForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetContactQuery() {
		assertEquals("findContNameQuery", contactRepositoryForLA.getContactQuery());
	}

	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndFirstNameAndLastNameFullMatchedAndPrimaryCountry() {
		final String firstName = "Jose David", lastName = "Miranda Echavarria", medicalId = "71774733", state = "", userId = "AGEA-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(1, contactList.size());
		assertContact(Iterables.getOnlyElement(contactList));
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndFirstNamePartialMatchedAndLastNamePartialMatchedAndPrimaryCountry() {
		final String firstName = "David", lastName = "Miranda Echav", medicalId = "71774733", state = "", userId = "AGEA-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(1, contactList.size());
		assertContact(Iterables.getOnlyElement(contactList));
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdNotFound() {
		final String firstName = "Jose David", lastName = "Miranda Echavarria", medicalId = "1234", state = "", userId = "AGEA-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByFirstNameNotFoundAndMedicalIdNotFound() {
		final String firstName = "Pedro", lastName = "Miranda Echavarria", medicalId = "1234", state = "", userId = "AGEA-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsWithoutFirstNameMatching() {
		final String firstName = "John Dante", lastName = "", medicalId = "71774733", state = "", userId = "AGEA-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByPrimaryCountryNotFound() {
		final String firstName = "Jose David", lastName = "Miranda Echavarria", medicalId = "71774733", state = "", userId = "FAKE-5DBM5Q";
		List<Map<String, Object>>  contactList = contactRepositoryForLA.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}


	private void assertContact(final Map<String, Object> contactMap) {
		assertEquals("Colombia", contactMap.get("PRIMARY_COUNTRY"));
		assertEquals("AGEA-4RY0UP", contactMap.get("CONTACTID"));
		assertEquals("Jose David", contactMap.get("CONTACTFIRSTNAME"));
		assertEquals("Miranda Echavarria", contactMap.get("CONTACTLASTNAME"));
		assertNull(contactMap.get("CONTACTEMAIL"));
		assertEquals("Physician", contactMap.get("CONTACTTYPE"));
		assertEquals("CO_CST_5430", contactMap.get("EXTID"));
		assertEquals("Active", contactMap.get("STATUS"));
		assertEquals("AGEA-4QLX16", contactMap.get("ACCOUNTID"));		
	}

}
