package com.example.connect.database.repository.timecycles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-mx-test.xml" })
public class TimeCyclesRepositoryServiceForMXTest {
	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForAddServiceForMX")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForAddServiceForMX;

	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForDeleteServiceForMX")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForDeleteServiceForMX;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForAddServiceForMX")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForAddServiceForMX;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForDeleteServiceForMX")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForDeleteServiceForMX;


	@BeforeClass
	public static void setSystemProps() {
		System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetWindowsAvailableForAddContact() {
		final String country = "Mexico";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);

		assertEquals(result.get("BUSINESS_UNIT"), businessUnit);

	}

	@Test
	public void testGetWindowsAvailableForAddContactNoFound() {

		final String country = "mx";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForAddAccount() {

		final String country = "Mexico";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);
		assertEquals(result.get("BUSINESS_UNIT"), businessUnit);

	}

	@Test
	public void testGetWindowsAvailableForAddAccountNoFound() {

		final String country = "MX";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForDelContact() {
		final String country = "Mexico";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertTrue(map.size() > 0);

	}

	@Test
	public void testGetWindowsAvailableForDelContactNoFound() {

		final String country = "MX";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForDelAccount() {
		final String country = "Mexico";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertTrue(map.size() > 0);
	}

	@Test
	public void testGetWindowsAvailableForDelAccountNoFound() {
		final String country = "MX";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForMX.getWindowsAvailable(country,
				businessUnit);

		assertNull(map);
	}

}
