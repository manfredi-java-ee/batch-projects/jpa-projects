package com.example.connect.database.repository.isales;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.connect.database.model.isales.Allocation;
import com.example.connect.database.model.isales.ContactTeam;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
		"classpath:connect-database-service-la-test.xml", 
		"classpath:connect-database-service-mx-test.xml",
		"classpath:connect-database-service-br-test.xml" })
@EnableTransactionManagement
@IntegrationTest
public class ModificationTrackingRepositoryServiceTest {
	@Autowired
	@Qualifier(value = "modificationTrackingRepositoryServiceForLA")
	private ModificationTrackingRepositoryService modificationTrackingRepositoryServiceForLA;

	@Autowired
	@Qualifier(value = "modificationTrackingRepositoryServiceForMX")
	private ModificationTrackingRepositoryService modificationTrackingRepositoryServiceForMX;

	@Autowired
	@Qualifier(value = "modificationTrackingRepositoryServiceForBR")
	private ModificationTrackingRepositoryService modificationTrackingRepositoryServiceForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindAllocationByActionAddForLA() {
		List<Allocation> list = modificationTrackingRepositoryServiceForLA.findAllocationByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("nueva alocacion-la", list.get(0).getAllocation());
	}
	
	@Test
	public void testFindContactTeamByActionAddForLA() {
		List<ContactTeam> list = modificationTrackingRepositoryServiceForLA.findContactTeamByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("contact1-la", list.get(0).getContactid());
		assertEquals("id-100-la", list.get(0).getId());
		assertEquals("testuser-la", list.get(0).getUserId());
	}

	@Test
	public void testLogForLA() {
		assertEquals(1L, modificationTrackingRepositoryServiceForLA.log("pid", "type", "error description", "error message"));
	}

	@Test
	public void testFindAllocationByActionAddForMX() {
		List<Allocation> list = modificationTrackingRepositoryServiceForMX.findAllocationByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("nueva alocacion-mx", list.get(0).getAllocation());
	}

	@Test
	public void testFindAllocationByActionAddForBR() {
		List<Allocation> list = modificationTrackingRepositoryServiceForBR.findAllocationByAction("ADD");
		assertEquals(2L, list.size());
		assertEquals("nueva alocacion-1-br", list.get(0).getAllocation());
		assertEquals("nueva alocacion-2-br", list.get(1).getAllocation());
	}
}
