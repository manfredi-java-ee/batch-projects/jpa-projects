package com.example.connect.database.repository.log;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@EnableTransactionManagement
public class TaskLogRepositoryTest {
	@Autowired
	@Qualifier(value = "taskLogRepositoryJDBCForLA")
	private TaskLogRepository repo;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Rollback(value = true)
	public void testSave() {
	 	
 
		 
		 
		final int records = repo.log("TASK","OBJECTID","ERROR","menasje de error","Aproval","Completed","ERRONEO");

		assertEquals(1, records);
	}
}