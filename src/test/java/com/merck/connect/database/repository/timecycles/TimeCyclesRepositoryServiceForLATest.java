package com.example.connect.database.repository.timecycles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
public class TimeCyclesRepositoryServiceForLATest {
	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForAddServiceForLA")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForAddServiceForLA;

	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForDeleteServiceForLA")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForDeleteServiceForLA;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForAddServiceForLA")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForAddServiceForLA;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForDeleteServiceForLA")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForDeleteServiceForLA;

	@BeforeClass
	public static void setSystemProps() {
		System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetWindowsAvailableForAddContact() throws ParseException {

		final String country = "CRO";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);

		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20400229");

		assertEquals(sf, result.get("CRE_APP_DT"));
		assertEquals("NA", result.get("BUSINESS_UNIT"));

	}

	@Test
	public void testGetWindowsAvailableForAddContactNoFound() {

		final String country = "mx";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForAddAccount() throws ParseException {

		final String country = "Ecuador";
		final String businessUnit = "NA";

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);

		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160229");

		assertEquals(sf, result.get("CRE_APP_DT_ACC"));
		assertEquals(result.get("BUSINESS_UNIT"), businessUnit);

	}

	@Test
	public void testGetWindowsAvailableForAddAccountNoFound() {

		final String country = "MX";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForDelContact() throws ParseException {

		final String country = "CRO";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertTrue(map.size() > 0);
		Map<String, Object> result = map.get(0);

		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160229");

		assertEquals(sf, result.get("DEL_APP_DT"));

	}

	@Test
	public void testGetWindowsAvailableForDelContactNoFound() {

		final String country = "MX";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertNull(map);

	}

	@Test
	public void testGetWindowsAvailableForDelAccount() throws ParseException {
		final String country = "CRO";
		final String businessUnit = "MSD BU TELEREPS VACUNAS";

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);
		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160229");

		assertEquals(sf, result.get("DEL_APP_DT_ACC"));

	}

	@Test
	public void testGetWindowsAvailableForDelAccountNoFound() {
		final String country = "MX";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForLA
				.getWindowsAvailable(country, businessUnit);

		assertNull(map);
	}

	@Test
	public void testGetWindowsAvailableForAddAccountEcuador() throws ParseException {
		final String country = "Ecuador";
		final String businessUnit = "NA";

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForLA
				.getWindowsAvailable(country, businessUnit);
		assertEquals(1, map.size());

		Map<String, Object> result = map.get(0);
		final Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160229");
		assertEquals(sf, result.get("CRE_APP_DT_ACC"));
		assertEquals("NA", result.get("BUSINESS_UNIT"));
	}

}
