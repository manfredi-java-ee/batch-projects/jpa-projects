package com.example.connect.database.repository.contact.team;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerBR")
@Rollback
public class ContactTeamRepositoryJDBCForBRTest {
	@Autowired
	@Qualifier(value = "contactTeamRepositoryJDBCForBR")
	private ContactTeamRepository contactTeamRepository;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testFindContactByDifferentSalesTeamWithEmptyResultSet() {
		final String contactExternalId = "BR-XXXXX";   
		final String userId = "AJIA-1CIOBG";
 		
		final List<String> salesTeams = new ArrayList<String>(); 
		salesTeams.add("RESPIRATORIA");
		salesTeams.add("MSD SEM FRONTEIRAS");

		final List<Map<String, Object>> contactList = contactTeamRepository.foundContactInOtherPanelByDifferentSalesTeam(
				contactExternalId, userId, salesTeams);
		assertTrue(contactList.isEmpty());
	}

 	@Test
	public void testFindContactByDifferentSalesTeam() {
		final String contactExternalId = "BR-0000478513";   
		final String userId = "AJIA-1CIOBG";
 		
		final List<String> salesTeams = new ArrayList<String>(); 
		salesTeams.add("RESPIRATORIA");
		salesTeams.add("MSD SEM FRONTEIRAS");
		
		final List<Map<String, Object>>  contactList = contactTeamRepository.foundContactInOtherPanelByDifferentSalesTeam(
 	 			contactExternalId, userId, salesTeams);
	
		final Map<String, Object> mapContact = contactList.get(0);
		assertEquals(1, contactList.size());
		assertEquals("RESPIRATORIA", mapContact.get("SALESTEAMID"));
		assertEquals("51109203", mapContact.get("EXTID"));
		assertEquals("BRUNA", mapContact.get("USERFIRSTNAME"));
		assertEquals("TACOLA DO AMARAL", mapContact.get("USERLASTNAME"));
	}
	
	@Test
	public void testFindContactBySameSalesTeamWithEmptyResultSet() {
		final String contactExternalId = "BR-XXXXX";   
		final String userId = "AJIA-1CIOBG";
		final String salesTeamId = "MSD SEM FRONTEIRAS";
		
		final List<Map<String, Object>>  contactList = contactTeamRepository.foundContactInOtherPanelBySameSalesTeam(contactExternalId, userId, salesTeamId);
	 	assertTrue(contactList.isEmpty());
	}

	@Test
	public void testFindContactBySameSalesTeam() {
		final String contactExternalId = "BR-0000478513";   
		final String userId = "AJIA-1CIOBG";
 		final String salesTeamId = "MSD SEM FRONTEIRAS";
		
 		final List<Map<String, Object>>  contactList = contactTeamRepository.foundContactInOtherPanelBySameSalesTeam(contactExternalId, userId, salesTeamId);
	 	final Map<String, Object> mapContact = contactList.get(0);
		
		assertEquals(1, contactList.size());
		assertEquals(salesTeamId, mapContact.get("SALESTEAMID"));
		assertEquals("51041247", mapContact.get("EXTID"));
		assertEquals("EVELYN", mapContact.get("USERFIRSTNAME"));
		assertEquals("ANDREA MELENDEZ VAZ", mapContact.get("USERLASTNAME"));
	}

}
