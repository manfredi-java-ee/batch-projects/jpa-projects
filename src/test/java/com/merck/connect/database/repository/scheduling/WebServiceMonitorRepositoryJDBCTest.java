package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.scheduling.WebServiceMonitor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class WebServiceMonitorRepositoryJDBCTest {

	@Autowired
	@Qualifier(value = "webServiceMonitorRepositoryJDBCForLA")
	private WebServiceMonitorRepository webServiceMonitorRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testFindByWsName() {
		WebServiceMonitor webServiceMonitorActual = this.webServiceMonitorRepositoryJDBCForLA.findByWsName("WS-PROCESSID-6");
		assertEquals("WS-PROCESSID-6", webServiceMonitorActual.getWsName());
		assertNull(webServiceMonitorActual.getDelItemName());
		assertNotNull(webServiceMonitorActual.getExDate());
		assertNotNull(webServiceMonitorActual.getSyncDate());
		assertEquals("03:10:25", webServiceMonitorActual.getDuration());
		assertEquals(99, webServiceMonitorActual.getRowsProcessed());
		assertEquals(11, webServiceMonitorActual.getRowsDeleted());
		assertEquals("SUCCESSFUL", webServiceMonitorActual.getExStatus());
		assertEquals("SUCCESSFUL", webServiceMonitorActual.getCrmStatus());
		assertEquals("SUCCESSFUL", webServiceMonitorActual.getWimpStatus());		
	}

	@Test
	public void testFindByWsNameNotSuccessfulStatuses() {
		WebServiceMonitor webServiceMonitorActual = this.webServiceMonitorRepositoryJDBCForLA.findByWsName("WS-PROCESSID-1");
		assertEquals("SUCCESSFUL", webServiceMonitorActual.getExStatus());
		assertEquals("IN_PROGRESS", webServiceMonitorActual.getCrmStatus());
		assertEquals("ERROR", webServiceMonitorActual.getWimpStatus());	
	}

	@Test
	public void testFindByWsNameNotSuccessfulWimpStatus() {
		assertNull(this.webServiceMonitorRepositoryJDBCForLA.findByWsName("WS-PROCESSID-2"));		
	}

	@Test
	public void testFindByWsNameNotSuccessfulCrmStatus() {
		assertNull(this.webServiceMonitorRepositoryJDBCForLA.findByWsName("WS-PROCESSID-3"));		
	}
	
	@Test
	public void testFindByWsNameNotFound() {
		assertNull(this.webServiceMonitorRepositoryJDBCForLA.findByWsName("WS-PROCESSID-NOT-FOUND"));
	}

}
