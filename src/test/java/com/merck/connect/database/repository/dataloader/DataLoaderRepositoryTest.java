package com.example.connect.database.repository.dataloader;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.connect.database.repository.dataloader.DataLoaderRepository;
import com.example.connect.database.model.dataloader.ParameterLoaderDB;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@EnableTransactionManagement
public class DataLoaderRepositoryTest {
	@Autowired 			
	@Qualifier(value = "dataLoaderRepositoryJDBCForLA")
	private DataLoaderRepository repo;
	
	@Before
	public void setUp() throws Exception {

	}

	
	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Rollback(value = true)
	public void testFindParameter() throws SQLException {
		
	final List<ParameterLoaderDB> records = repo.findParameter();
		ParameterLoaderDB parm = records.get(0);
		assertEquals("LA-002", parm.getId());
	}
	
	@Test
	@Rollback(value = true)
	public void testFindExtraction() throws SQLException {
		
	final ResultSet records = repo.findExtraction("SELECT * FROM SODM_REPORT where region = $$Region$$", "BR", "Insert");
		records.next();
		assertEquals("1615", records.getString("ID"));
	}
	
	@Test
	@Rollback(value = true)
	public void testInfoRowProcess() throws SQLException {
		assertEquals(1, repo.infoRowProcess("LA-002", 1, "xx", "ERROR"));
	}
}