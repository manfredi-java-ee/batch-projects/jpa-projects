package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.scheduling.ProcessType;
import com.example.connect.database.model.scheduling.TaskActive;
import com.example.connect.database.model.scheduling.TaskProcess;
import com.example.connect.database.model.scheduling.TaskStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class SchedulerRepositoryJDBCTest {
	@Autowired
	@Qualifier(value = "schedulerRepositoryJDBCForLA")
	private SchedulerRepository schedulerRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testAreAllProcessesFinished() {
		assertEquals(2, this.schedulerRepositoryJDBCForLA.areAllProcessesFinished("HOURLY", "LA").size());
	}

	@Test
	public void testFindTasksToRestartByRegionNotFound() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findTasksToRestartByRegion(
				"UNKNOWN-REGION", TaskActive.Y);
		assertTrue(processes.isEmpty());
	}

	@Test
	public void testFindTasksToRestartByRegion() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findTasksToRestartByRegion("LA", TaskActive.Y);
		assertEquals(2, processes.size());
		
		TaskProcess processActual = processes.get(0);
		assertEquals("WS-PROCESSID-7", processActual.getProcessId());
		assertEquals(TaskStatus.RESTART, processActual.getStatus());
		assertEquals("SodMasterWS-1.exe", processActual.getApplicationName());
		assertEquals(1, processActual.getSequentialOrder());
		assertEquals("Hourly Web Service Process #7", processActual.getDescription());

		processActual = processes.get(1);
		assertEquals("WS-PROCESSID-8", processActual.getProcessId());
		assertEquals(TaskStatus.RESTART, processActual.getStatus());
		assertEquals("SodMasterWS-2.exe", processActual.getApplicationName());
		assertEquals(2, processActual.getSequentialOrder());
		assertEquals("Hourly Web Service Process #8", processActual.getDescription());
	}

	@Test
	public void testFindByTaskName() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findByTaskName(
				"DAILY", "LA", TaskActive.Y);
		assertEquals(1, processes.size());
		TaskProcess processActual = processes.get(0);
		assertEquals(ProcessType.JAVA, processActual.getType());
		assertEquals("PROCESSID-1", processActual.getProcessId());
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals("/APP-REPO", processActual.getLocation());
		assertEquals("connect-isales-modification-tracking", processActual.getApplicationName());
		assertEquals("DAILY LA connect-isales-modification-tracking", processActual.getArguments());
		assertEquals("Y", processActual.getRep());
		assertEquals(1, processActual.getSequentialOrder());
		assertEquals(25, processActual.getRowsProcessed());
		assertEquals(12, processActual.getRowsDeleted());
		assertEquals(null, processActual.getDuration());
		assertEquals(null, processActual.getStart());
		assertEquals(null, processActual.getEnd());
		assertEquals(TaskActive.Y, processActual.getActive());
		assertEquals("Daily Java Process #1", processActual.getDescription());
	}

	@Test
	public void testFindByTaskNameWithSequentialOrder() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findByTaskName(
				"NIGHTLY", "LA", TaskActive.Y);
		assertEquals(4, processes.size());

		TaskProcess processActual = processes.get(0);
		assertEquals("PROCESSID-2", processActual.getProcessId());
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals("connect-isales-modification-tracking", processActual.getApplicationName());
		assertEquals(1, processActual.getSequentialOrder());
		assertEquals("Nightly Java Process #2", processActual.getDescription());
		
		processActual = processes.get(1);
		assertEquals("PROCESSID-3", processActual.getProcessId());
		assertEquals(TaskStatus.IN_PROGRESS, processActual.getStatus());
		assertEquals("connect-ws-soap-client", processActual.getApplicationName());
		assertEquals(2, processActual.getSequentialOrder());
		assertEquals("Nightly Java Process #3", processActual.getDescription());
		
		processActual = processes.get(2);
		assertEquals("PROCESSID-4", processActual.getProcessId());
		assertEquals(TaskStatus.ERROR, processActual.getStatus());
		assertEquals("connect-ws-service", processActual.getApplicationName());
		assertEquals(3, processActual.getSequentialOrder());
		assertEquals("Nightly Java Process #4", processActual.getDescription());
		
		processActual = processes.get(3);
		assertEquals("WS-PROCESSID-6", processActual.getProcessId());
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals("SodMasterWS.exe", processActual.getApplicationName());
		assertEquals(5, processActual.getSequentialOrder());
		assertEquals("Nightly Java Process #6", processActual.getDescription());
	}
	
	@Test
	public void testFindByTaskNameInactive() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findByTaskName(
				"NIGHTLY", "LA", TaskActive.N);
		assertEquals(1, processes.size());

		TaskProcess processActual = processes.get(0);
		assertEquals("PROCESSID-5", processActual.getProcessId());
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals("connect-APP-INACTIVE", processActual.getApplicationName());
		assertEquals(4, processActual.getSequentialOrder());
		assertEquals("Nightly Java Process #5", processActual.getDescription());
	}

	@Test
	public void testFindByTaskNameNotFound() {
		final List<TaskProcess> processes = this.schedulerRepositoryJDBCForLA.findByTaskName(
				"DAILY-NON-EXISTENT", "LA", TaskActive.Y);
		assertTrue(processes.isEmpty());
	}
	
	@Test
	public void testFindByTaskNameAndProcessId() {
		final TaskProcess processActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId(
				"DAILY", "LA", "PROCESSID-1");
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals("/APP-REPO", processActual.getLocation());
		assertEquals("DAILY LA connect-isales-modification-tracking", processActual.getArguments());
		assertEquals("Y", processActual.getRep());
		assertEquals(1, processActual.getSequentialOrder());
		assertEquals(25, processActual.getRowsProcessed());
		assertEquals(null, processActual.getDuration());
		assertEquals(null, processActual.getStart());
		assertEquals(null, processActual.getEnd());
		assertEquals("Daily Java Process #1", processActual.getDescription());
	}

	@Test
	public void testFindByTaskNameAndApplicationNameNotFound() {
		assertNull(this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId("DAILY", "LA", "app-not-found"));
	}

	@Test
	@Transactional
	@Rollback(value = true)
	public void testUpdateActiveTasksToInProcess() {
		final int updated = this.schedulerRepositoryJDBCForLA.updateActiveTasksToInProcess("DAILY", "LA");
		assertEquals(1, updated);

		final TaskProcess processActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId(
				"DAILY", "LA", "PROCESSID-1");
		assertEquals(TaskStatus.IN_PROGRESS, processActual.getStatus());
	}

	@Test
	public void testUpdateActiveTasksToInProcessNotFound() {
		assertEquals(0, this.schedulerRepositoryJDBCForLA.updateActiveTasksToInProcess("TASK-INEXISTENT", "LA"));
	}

	@Test
	@Transactional
	@Rollback(value = true)
	public void testDisableRestartProcess() {
		TaskProcess tp = new TaskProcess();
		tp.setTaskName("HOURLY");
		tp.setRegion("LA");
		tp.setProcessId("WS-PROCESSID-7");
		tp.setArguments("arg1");

		final int updated = this.schedulerRepositoryJDBCForLA.disableRestartProcess(tp);
		assertEquals(1, updated);

		final TaskProcess updatedProcessActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId(
				"HOURLY", "LA", "WS-PROCESSID-7");
		assertEquals(TaskStatus.IN_PROGRESS, updatedProcessActual.getStatus());
		assertNull(updatedProcessActual.getStart());
	}

	@Test
	@Transactional
	@Rollback(value = true)
	public void testUpdateStartTime() {
		final Timestamp startTime = new Timestamp(new Date().getTime());
		final String processId = "PROCESSID-1";
		final int updated = this.schedulerRepositoryJDBCForLA.updateStartTime("DAILY", "LA", processId, startTime);
		assertEquals(1, updated);

		final TaskProcess processActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId(
				"DAILY", "LA", processId);
		assertEquals(TaskStatus.COMPLETED, processActual.getStatus());
		assertEquals(startTime, processActual.getStart());
	}

	@Test
	public void testUpdateStartTimeNotFound() {
		assertEquals(0, this.schedulerRepositoryJDBCForLA.updateStartTime(
				"DAILY", "LA", "app-not-found", new Timestamp(new Date().getTime())));
	}

	@Test
	@Transactional
	@Rollback(value = true)
	public void testUpdateEndProcess() {
		final Timestamp finishTime = new Timestamp(new Date().getTime());
		final String processId = "PROCESSID-1";
		final int updated = this.schedulerRepositoryJDBCForLA.updateEndProcess(
				"DAILY", "LA", processId, TaskStatus.ERROR, finishTime, 
				"00:00:01", "There is an error during the process");
		assertEquals(1, updated);

		final TaskProcess processActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId
				("DAILY", "LA", processId);
		assertEquals(TaskStatus.ERROR, processActual.getStatus());
		assertEquals("There is an error during the process", processActual.getErrorMessage());
		assertEquals("00:00:01", processActual.getDuration());
		assertEquals("Daily Java Process #1", processActual.getDescription());
		assertEquals(null, processActual.getStart());
		assertEquals(finishTime, processActual.getEnd());
	}

	@Test
	public void testUpdateEndProcessNotFound() {
		assertEquals(0, this.schedulerRepositoryJDBCForLA.updateEndProcess(
				"DAILY", "LA", "app-not-found", TaskStatus.COMPLETED, 
				new Timestamp(new Date().getTime()), "00:00:01", null));
	}
	

	@Test
	@Transactional
	@Rollback(value = true)
	public void testUpdateRowsProcessed() {
		final String processId = "PROCESSID-1";
		final int updated = this.schedulerRepositoryJDBCForLA.updateRowsProcessed(
				"DAILY", "LA", processId, 5, 2);
		assertEquals(1, updated);

		final TaskProcess processActual = this.schedulerRepositoryJDBCForLA.findByTaskNameAndProcessId(
				"DAILY", "LA", processId);
		assertEquals(5, processActual.getRowsProcessed());
		assertEquals(2, processActual.getRowsDeleted());
	}
}
