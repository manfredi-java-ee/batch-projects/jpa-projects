package com.example.connect.database.repository.account;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerBR")
@Rollback
public class AccountRepositoryJDBCForBRTest {

	@Autowired
	@Qualifier(value = "accountRepositoryJDBCForBR")
	private AccountRepository accountRepositoryForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetAccountQuery() {
		assertEquals("findAccCNPJQuery", accountRepositoryForBR.getAccountQuery());
	}

	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String accountName = "", colony = "", cnpj = "";
		final List<Map<String, Object>>  contactList = accountRepositoryForBR.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByCNPJWithExtIdForBR() {
		final String accountName = "", colony = "", cnpj = "61412110050500";
		final List<Map<String, Object>>  accountList = accountRepositoryForBR.findByCustomFields(accountName, colony, cnpj);

		assertEquals(1, accountList.size());
		assertAccount(Iterables.getOnlyElement(accountList));
	}

	@Test
	public void testFindByCustomFieldsByCNPJAndExtIdWithoutBRPrefix() {
		final String accountName = "", colony = "", cnpj = "99912110050588";
		final List<Map<String, Object>>  accountList = accountRepositoryForBR.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}
	
	@Test
	public void testFindByCustomFieldsByCNPJAndExtIdWithNullValue() {
		final String accountName = "", colony = "", cnpj = "77766610050588";
		final List<Map<String, Object>>  accountList = accountRepositoryForBR.findByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountList.size());
	}
	
	private void assertAccount(final Map<String, Object> accountMap) {
		assertEquals("AJIA-7957WC", accountMap.get("ACCOUNTID"));
		assertEquals("CP. EVELINE CAMPOS MONTEIRO DE CASTRO.", accountMap.get("ACCOUNTNAME"));
		assertEquals("Doctors Office", accountMap.get("ACCOUNTTYPE"));
		assertEquals("AJIA-3TZ3WN", accountMap.get("USERID"));
		assertEquals("BR-0000809010", accountMap.get("EXTID"));
		assertEquals("Active", accountMap.get("STATUS"));
		assertEquals("61412110050500", accountMap.get("CNPJ"));
	}

}
