package com.example.connect.database.repository.timecycles;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerBR")
@Rollback
public class TimeCyclesRepositoryForContactForDeleteJDBCForBRTest {

	@Autowired
	@Qualifier(value = "timeCyclesRepositoryForContactForDeleteJDBCForBR")
	private TimeCyclesRepository timeCyclesRepositoryForContactForDeleteJDBCForBR;

	@BeforeClass
	public static void setSystemProps() {
		System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetWindows() throws Exception {
		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteJDBCForBR.getWindows();
		assertEquals(1, map.size());

		final Map<String, Object> result = map.get(0);
		assertEquals("Brasil", result.get("COUNTRY"));
		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160222");
		assertEquals(sf, result.get("DEL_APP_DT"));
		assertEquals("NA", result.get("BUSINESS_UNIT"));
		assertEquals("2016", result.get("CYCLEYEAR"));
		assertEquals("02", result.get("CYCLEID"));
		sf = new SimpleDateFormat("yyyyMMdd").parse("20160510");
		assertEquals(sf, result.get("DEL_STA_DT_ACC"));
		sf = new SimpleDateFormat("yyyyMMdd").parse("20500320");
		assertEquals(sf, result.get("DEL_END_DT_ACC"));
		sf = new SimpleDateFormat("yyyyMMdd").parse("20160320");
		assertEquals(sf, result.get("DEL_APP_DT_ACC"));
	}

}
