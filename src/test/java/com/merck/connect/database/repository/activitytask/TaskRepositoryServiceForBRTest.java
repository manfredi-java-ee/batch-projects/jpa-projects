package com.example.connect.database.repository.activitytask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
public class TaskRepositoryServiceForBRTest {	
	@Autowired
	@Qualifier("taskRepositoryServiceForBR")
	private TaskRepositoryService taskRepositoryServiceForBR;


	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}
	
	@Test
	public void testFindContactByCustomFields() {
		String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		assertTrue(taskRepositoryServiceForBR.findContactByCustomFields(firstName, lastName, medicalId, state, userId).isEmpty());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNameFullMatched() {
		final String firstName = "", lastName = "CAMILA DELFINO", medicalId = "0119173", state = "SP", userId = "";
		final Map<String, Object> contactMap = taskRepositoryServiceForBR.findContactByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(78, contactMap.size());
		assertEquals("BR-0000557248", contactMap.get("EXTID"));
	}

	@Test
	public void testFindAccountByCustomFieldsNotFound() {
		final String accountName = "", colony = "", cnpj = "";
		assertTrue(taskRepositoryServiceForBR.findAccountByCustomFields(accountName, colony, cnpj).isEmpty());
	}

	@Test
	public void testFindAccountByCustomFieldsByCNPJ() {
		final String accountName = "", colony = "", cnpj = "61412110050500";
		final Map<String, Object> contactMap = taskRepositoryServiceForBR.findAccountByCustomFields(accountName, colony, cnpj);

		assertEquals(37, contactMap.size());
		assertEquals("BR-0000809010", contactMap.get("EXTID"));
	}

}
