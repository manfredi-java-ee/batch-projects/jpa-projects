package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class DashboardMonitorRepositoryJDBCTest {

	@Autowired
	@Qualifier(value = "dashboardMonitorRepositoryJDBCForLA")
	private DashboardMonitorRepository dashboardMonitorRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testUpdateEnableByRegion() {
		assertEquals(1, this.dashboardMonitorRepositoryJDBCForLA.updateEnableByRegion("LA", 0));
		assertEquals(1, this.dashboardMonitorRepositoryJDBCForLA.updateEnableByRegion("LA", 1));
	}

	@Test
	public void testUpdateEnableByRegionNotFound() {
		assertEquals(0, this.dashboardMonitorRepositoryJDBCForLA.updateEnableByRegion("UK", 0));
	}

}
