package com.example.connect.database.repository.isales;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.connect.database.model.isales.ContactTeam;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
		"classpath:connect-database-service-la-test.xml", 
		"classpath:connect-database-service-mx-test.xml",
		"classpath:connect-database-service-br-test.xml" })
public class ContactTeamRepositoryTest {

	@Autowired
	@Qualifier(value = "contactTeamIsalesRepositoryJDBCForLA")
	private ContactTeamRepository contactTeamIsalesRepositoryForLA;

	@Autowired
	@Qualifier(value = "contactTeamIsalesRepositoryJDBCForMX")
	private ContactTeamRepository contactTeamIsalesRepositoryForMX;

	@Autowired
	@Qualifier(value = "contactTeamIsalesRepositoryJDBCForBR")
	private ContactTeamRepository contactTeamIsalesRepositoryForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindByActionAddForLA() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForLA.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("contact1-la", list.get(0).getContactid());
		assertEquals("id-100-la", list.get(0).getId());
		assertEquals("testuser-la", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForLA() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForLA.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("contact2-la", list.get(0).getContactid());
		assertEquals("id-200-la", list.get(0).getId());
		assertEquals("testuser-la", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionAddForMX() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForMX.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("contact1-mx", list.get(0).getContactid());
		assertEquals("id-100-mx", list.get(0).getId());
		assertEquals("testuser-mx", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForMX() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForMX.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("contact2-mx", list.get(0).getContactid());
		assertEquals("id-200-mx", list.get(0).getId());
		assertEquals("testuser-mx", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionAddForBR() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForBR.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("contact1-br", list.get(0).getContactid());
		assertEquals("id-100-br", list.get(0).getId());
		assertEquals("testuser-br", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForBR() {
		List<ContactTeam> list = contactTeamIsalesRepositoryForBR.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("contact2-br", list.get(0).getContactid());
		assertEquals("id-200-br", list.get(0).getId());
		assertEquals("testuser-br", list.get(0).getUserId());
	}

}
