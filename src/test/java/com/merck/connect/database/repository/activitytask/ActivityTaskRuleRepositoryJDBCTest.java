package com.example.connect.database.repository.activitytask;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.activitytask.ActivityTaskRule;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class ActivityTaskRuleRepositoryJDBCTest {
	@Autowired
	@Qualifier(value = "activityTaskRuleRepositoryJDBCForLA")
	private ActivityTaskRuleRepository activityTaskRuleRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetBusinessRule() {
		List<ActivityTaskRule> list = activityTaskRuleRepositoryJDBCForLA.getBusinessRule();
		assertThat(list.size(), is(7));
	}

}
