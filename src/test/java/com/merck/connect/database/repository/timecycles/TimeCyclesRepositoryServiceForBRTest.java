package com.example.connect.database.repository.timecycles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-br-test.xml" })
public class TimeCyclesRepositoryServiceForBRTest {
	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForAddServiceForBR")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForAddServiceForBR;

	@Autowired
	@Qualifier("timeCyclesRepositoryForContactForDeleteServiceForBR")
	private TimeCyclesRepositoryService timeCyclesRepositoryForContactForDeleteServiceForBR;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForAddServiceForBR")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForAddServiceForBR;

	@Autowired
	@Qualifier("timeCyclesRepositoryForAccountForDeleteServiceForBR")
	private TimeCyclesRepositoryService timeCyclesRepositoryForAccountForDeleteServiceForBR;

	@BeforeClass
	public static void setSystemProps() {
		System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetWindowsAvailableForAddContact() throws ParseException {
		final String country = "Brasil";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);
		assertEquals(result.get("BUSINESS_UNIT"), "NA");
		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20161017");
		assertEquals(sf, result.get("CRE_APP_DT"));
	}

	@Test
	public void testGetWindowsAvailableForAddContactNoFound() {
		final String country = "BR";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForAddServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertNull(map);
	}

	@Test
	public void testGetWindowsAvailableForAddAccount() throws ParseException {
		final String country = "Brasil";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertTrue(map.size() > 0);

		Map<String, Object> result = map.get(0);
		assertEquals(result.get("BUSINESS_UNIT"), "NA");
		Date sf = new SimpleDateFormat("yyyyMMdd").parse("20160320");
		assertEquals(sf, result.get("CRE_APP_DT_ACC"));
	}

	@Test
	public void testGetWindowsAvailableForAddAccountNoFound() {
		final String country = "BR";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForAddServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertNull(map);
	}

	@Test
	public void testGetWindowsAvailableForDelContact() {
		final String country = "Brasil";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertTrue(map.size() > 0);
	}

	@Test
	public void testGetWindowsAvailableForDelContactNoFound() {
		final String country = "BR";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForContactForDeleteServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertNull(map);
	}

	@Test
	public void testGetWindowsAvailableForDelAccount() {
		final String country = "Brasil";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertTrue(map.size() > 0);
	}

	@Test
	public void testGetWindowsAvailableForDelAccountNoFound() {
		final String country = "BR";
		final String businessUnit = null;

		final List<Map<String, Object>> map = timeCyclesRepositoryForAccountForDeleteServiceForBR.getWindowsAvailable(country,
				businessUnit);
		assertNull(map);
	}

}
