package com.example.connect.database.repository.starbyproduct;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class StarByProductRepositoryJDBCForLATest {

	@Autowired
	@Qualifier(value = "starByProductRepositoryJDBCForLA")
	private StarByProductRepository starByProductRepositoryJDBCForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testIsAuditWithUserAndContactNotFound() {
		final String userId = "dummy-user-id"; 
		final String contactId = "dummy-contact-id";

		assertThat(this.starByProductRepositoryJDBCForLA.isAudit(userId, contactId), is(Boolean.FALSE));
	}

	@Test
	public void testIsAuditWithUserAndContactNull() {
		final String userId = null; 
		final String contactId = null;

		assertThat(this.starByProductRepositoryJDBCForLA.isAudit(userId, contactId), is(Boolean.FALSE));
	}

	@Test
	public void testIsAuditWithUserAndContactEmpty() {
		final String userId = ""; 
		final String contactId = "   ";

		assertThat(this.starByProductRepositoryJDBCForLA.isAudit(userId, contactId), is(Boolean.FALSE));
	}

	@Test
	public void testIsAuditWithUserAndContactFound() {
		final String userId = "AGEA-645VDH"; 
		final String contactId = "AGEA-516Q2T";
	
		assertThat(this.starByProductRepositoryJDBCForLA.isAudit(userId, contactId), is(Boolean.TRUE));
	}

}
