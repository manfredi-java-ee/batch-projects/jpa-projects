package com.example.connect.database.repository.isales;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.connect.database.model.isales.Allocation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:connect-database-service-la-test.xml", 
		"classpath:connect-database-service-mx-test.xml",
		"classpath:connect-database-service-br-test.xml" })
public class AllocationRepositoryTest {
	@Autowired
	@Qualifier(value = "allocationIsalesRepositoryJDBCForLA")
	private AllocationRepository allocationIsalesRepositoryJDBCForLA;

	@Autowired
	@Qualifier(value = "allocationIsalesRepositoryJDBCForMX")
	private AllocationRepository allocationIsalesRepositoryJDBCForMX;

	@Autowired
	@Qualifier(value = "allocationIsalesRepositoryJDBCForBR")
	private AllocationRepository allocationIsalesRepositoryJDBCForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindByActionAddForLA() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForLA.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("nueva alocacion-la", list.get(0).getAllocation());
	}

	@Test
	public void testFindByActionDelForLA() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForLA.findByAction("DEL"); 
		assertEquals(1L, list.size());
		assertEquals("borrada alocacion-la", list.get(0).getAllocation());
	}

	@Test
	public void testFindByActionAddForMX() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForMX.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("nueva alocacion-mx", list.get(0).getAllocation());
	}

	@Test
	public void testFindByActionDelForMX() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForMX.findByAction("DEL"); 
		assertEquals(1L, list.size());
		assertEquals("borrada alocacion-mx", list.get(0).getAllocation());
	}

	@Test
	public void testFindByActionAddForBR() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForBR.findByAction("ADD");
		assertEquals(2L, list.size());
		assertEquals("nueva alocacion-1-br", list.get(0).getAllocation());
		assertEquals("nueva alocacion-2-br", list.get(1).getAllocation());
	}

	@Test
	public void testFindByActionDelForBR() {
		List<Allocation> list = allocationIsalesRepositoryJDBCForBR.findByAction("DEL"); 
		assertEquals(1L, list.size());
		assertEquals("borrada alocacion-br", list.get(0).getAllocation());
	}
}
