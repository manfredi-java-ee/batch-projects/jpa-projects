package com.example.connect.database.repository.multilanguaje;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.connect.database.model.languaje.Multilanguaje;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
public class MultilanguajeRepositoryServiceTest {	
	@Autowired
	@Qualifier("multilanguajeRepositoryServiceForLA")
	private MultilanguajeRepositoryService multilanguajeRepositoryService;

	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}
	
 
	@Test
	public void testEncodingMessageByIdEspanish() throws ParseException {	 	
		final String idmessage = "BR001_CONTACT_ERROR_MESSAGE";
		final String country = "Argentina";
 	 	
		final Multilanguaje m = multilanguajeRepositoryService.getTraslationMessagesById(idmessage, country);
 
		
		assertNotNull(m);
		assertEquals("Los siguientes campos requeridos para la afiliación de un Contacto a un Panel no se han completado: %s (BR001).",
				m.getTraslation());
		
	 	 	  
	}

	@Test
	public void testFoundMessageByIdEspanish() throws ParseException {	 	
		final String idmessage = "VC001_VALIDATOR_CONTACT_ERROR_MESSAGE";
		final String country = "Argentina";
 	 	
		final Multilanguaje m = multilanguajeRepositoryService.getTraslationMessagesById(idmessage, country);
 
		
		assertNotNull(m);
		assertEquals("El contacto seleccionado en la Tarea no existe en MSD Connect (VC001).",m.getTraslation());
		
	 	 	  
	}
	
	@Test
	public void testFoundMessageByIdEnglish() throws ParseException {
		
	 	
		final String idmessage = "VC005_VALIDATOR_SELECTED_ERROR_MESSAGE";
		final String country = "Caribe";
 	 	
		final Multilanguaje m = multilanguajeRepositoryService.getTraslationMessagesById(idmessage, country);
 
		
		assertNotNull(m);
		assertEquals("The task are not account and contact selected  (VC005).",m.getTraslation());
		
	 	 	  
	}
	
	
	
	@Test
	public void testNotFoundMessageById() throws ParseException {
		
	 	
		final String idmessage = "VC001XX_VALIDATOR_CONTACT_ERROR_MESSAGE";
		final String country = "Argentina";
 	 	
		final Multilanguaje m = multilanguajeRepositoryService.getTraslationMessagesById(idmessage, country);
 
		
		assertNull(m);
   }
	
	
	
	@Test
	public void testNotFoundMessageByIdAndCountry() throws ParseException {
		
	 	
		final String idmessage = "VC005_VALIDATOR_SELECTED_ERROR_MESSAGE";
		final String country = "Canada";
 	 	
		final Multilanguaje m = multilanguajeRepositoryService.getTraslationMessagesById(idmessage, country);
 
		
		assertNull(m);
 	 	 	  
	}
	
	


	@Test
	public void testFoundFieldsByIdEspanish() throws ParseException {
		
	 	
		final List<String> fieldsToTraslate = new ArrayList<String>();
		
		fieldsToTraslate.add("addChangeContactType");
		fieldsToTraslate.add("addChangeFirstName");
 
	 	
	 	final String country = "Argentina";
 	 	
		final String fieldsTraslate = multilanguajeRepositoryService.getTraslationFields(fieldsToTraslate, country);
	 	 
		assertNotNull(fieldsTraslate);
		assertEquals("[Agregar/cambiar tipo de contacto, Primer Nombre Contacto]",fieldsTraslate);
 	 	 	  
	}
	
	
	@Test
	public void testFoundFieldsByIdEnglish() throws ParseException {
		
	 	
		final List<String> fieldsToTraslate = new ArrayList<String>();
		
		fieldsToTraslate.add("addChangeContactType");
		fieldsToTraslate.add("addChangeFirstName");
        fieldsToTraslate.add("addChangeLastName");
	 	
	 	final String country = "Caribe";
 	 	
		final String fieldsTraslate = multilanguajeRepositoryService.getTraslationFields(fieldsToTraslate, country);
	 	 
		assertNotNull(fieldsTraslate);
		assertEquals("[add/change type account, First name, Last Name]",fieldsTraslate);
		
	 	 	  
	}
	
	
	
	
	@Test
	public void testNotFoundFieldsById() throws ParseException {
		
		
		String MESSAGEFAILURE = "The traslation for field addChangecaxxaaxxa not found for country : Argentina";
		
	 	
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(MESSAGEFAILURE);
		
	 	
		final List<String> fieldsToTraslate = new ArrayList<String>();
		
		fieldsToTraslate.add("addChangeContactType");
		fieldsToTraslate.add("addChangeFirstName");
        fieldsToTraslate.add("addChangecaxxaaxxa");
	 	
	 	final String country = "Argentina";
 	 	
		multilanguajeRepositoryService.getTraslationFields(fieldsToTraslate, country);
	 	 
		 
   }
	
	@Test
	public void testNotFoundFieldsByIdAndCountry() throws ParseException {
		
	 
		final List<String> fieldsToTraslate = new ArrayList<String>();
		
		fieldsToTraslate.add("addChangeContactType");
		fieldsToTraslate.add("addChangeFirstName");
 	 	
	 	final String country = "Canada";
 	 	
		String result = multilanguajeRepositoryService.getTraslationFields(fieldsToTraslate, country);
	 	 
		assertNull(result);

   }
	
	
	
	
	
	
	 
	 
	 
 	 
	
 

 

}
