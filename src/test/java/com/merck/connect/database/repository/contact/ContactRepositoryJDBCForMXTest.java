package com.example.connect.database.repository.contact;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-mx-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerMX")
@Rollback
public class ContactRepositoryJDBCForMXTest {

	@Autowired
	@Qualifier(value = "contactRepositoryJDBCForMX")
	private ContactRepository contactRepositoryForMX;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetContactQuery() {
		assertEquals("findContMedIdandNameQuery", contactRepositoryForMX.getContactQuery());
	}

	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByFirstNameAndLastNameFullMatched() {
		final String firstName = "AGUILAR SANCHEZ", lastName = "GILBERTO ANDRES", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(1, contactList.size());
		assertContact(Iterables.getOnlyElement(contactList));
	}

	@Test
	public void testFindByCustomFieldsByFirstNameAndLastNamePartialMatched() {
		final String firstName = "AGUILAR", lastName = "GILBERTO", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByFirstNameFullMatchedAndLastNamePartialMatched() {
		final String firstName = "AGUILAR SANCHEZ", lastName = "GILBERTO", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByFirstNameAndLastNameWithExtIdNull() {
		final String firstName = "JUAN MANUEL", lastName = "VARGAS ESPINOSA", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}

	@Test
	public void testFindByCustomFieldsByFirstNameAndLastNameWithExtIdNotMatchingMXPrefix() {
		final String firstName = "ERIKA GRACIELA", lastName = "FLORES PRECIADO", medicalId = "", state = "", userId = "";
		List<Map<String, Object>>  contactList = contactRepositoryForMX.findByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(0, contactList.size());
	}


	private void assertContact(final Map<String, Object> contactMap) {
		assertEquals("Mexico", contactMap.get("COUNTRY"));
		assertEquals("AJIA-B96P3G", contactMap.get("CONTACTID"));
		assertEquals("AGUILAR SANCHEZ", contactMap.get("CONTACTFIRSTNAME"));
		assertEquals("GILBERTO ANDRES", contactMap.get("CONTACTLASTNAME"));
		assertEquals("aguilarsanchez.gilberto@example.com", contactMap.get("CONTACTEMAIL"));
		assertEquals("Physician", contactMap.get("CONTACTTYPE"));
		assertEquals("MX-0000246632", contactMap.get("EXTID"));
		assertEquals("Active", contactMap.get("STATUS"));
		assertEquals("AJIA-BAUQV5", contactMap.get("ACCOUNTID"));
	}

}
