package com.example.connect.database.repository.log;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.connect.database.model.log.ProcessLog;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@EnableTransactionManagement
public class ProcessLogRepositoryTest {
	@Autowired
	@Qualifier(value = "processLogRepositoryJDBCForLA")
	private ProcessLogRepository repo;
	
	@Before
	public void setUp() throws Exception {
	}

	
	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Rollback(value = true)
	public void testSave() {
		ProcessLog processLog = new ProcessLog();
		processLog.setProcessId("ALL-PROC");
		processLog.setType("INFO");	
		processLog.setCd("SUMMARY");
		processLog.setMessage("Registros Procesados: 1: 0 OK - 1 Erroneos");
		processLog.setLogDate(new Date());
		final int records = repo.log("ALL-PROC", "INFO", "SUMMARY", "Registros Procesados: 1: 0 OK - 1 Erroneos");

		assertEquals(1, records);
	}
}