package com.example.connect.database.repository.isales;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.connect.database.model.isales.AccountTeam;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
		"classpath:connect-database-service-la-test.xml", 
		"classpath:connect-database-service-mx-test.xml",
		"classpath:connect-database-service-br-test.xml" })
public class AccountTeamRepositoryTest {

	@Autowired
	@Qualifier(value = "accountTeamIsalesRepositoryJDBCForLA")
	private AccountTeamRepository accountTeamIsalesRepositoryForLA;

	@Autowired
	@Qualifier(value = "accountTeamIsalesRepositoryJDBCForMX")
	private AccountTeamRepository accountTeamIsalesRepositoryForMX;

	@Autowired
	@Qualifier(value = "accountTeamIsalesRepositoryJDBCForBR")
	private AccountTeamRepository accountTeamIsalesRepositoryForBR;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindByActionAddForLA() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForLA.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("account1-la", list.get(0).getAccountid());
		assertEquals("id-100-la", list.get(0).getId());
		assertEquals("testuser-la", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForLA() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForLA.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("account2-la", list.get(0).getAccountid());
		assertEquals("id-200-la", list.get(0).getId());
		assertEquals("testuser-la", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionAddForMX() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForMX.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("account1-mx", list.get(0).getAccountid());
		assertEquals("id-100-mx", list.get(0).getId());
		assertEquals("testuser-mx", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForMX() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForMX.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("account2-mx", list.get(0).getAccountid());
		assertEquals("id-200-mx", list.get(0).getId());
		assertEquals("testuser-mx", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionAddForBR() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForBR.findByAction("ADD");
		assertEquals(1L, list.size());
		assertEquals("account1-br", list.get(0).getAccountid());
		assertEquals("id-100-br", list.get(0).getId());
		assertEquals("testuser-br", list.get(0).getUserId());
	}

	@Test
	public void testFindByActionDelForBR() {
		List<AccountTeam> list = accountTeamIsalesRepositoryForBR.findByAction("DEL");
		assertEquals(1L, list.size());
		assertEquals("account2-br", list.get(0).getAccountid());
		assertEquals("id-200-br", list.get(0).getId());
		assertEquals("testuser-br", list.get(0).getUserId());
	}

}
