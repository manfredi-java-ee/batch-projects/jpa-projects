package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class DashboardMonitorRepositoryJDBCServiceTest {

	@Autowired
	@Qualifier(value = "dashboardMonitorRepositoryServiceForLA")
	private  DashboardMonitorRepositoryService schedulerRepositoryServiceForLA;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testUpdateEnableByRegion() {
		assertEquals(1, this.schedulerRepositoryServiceForLA.updateEnableByRegion("LA", 0));		
	}

	@Test
	public void testUpdateEnableByRegionWithInvalidEnableValueGreaterThanOne() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The enable value has to be 0 (zero) or 1 (one)");

		this.schedulerRepositoryServiceForLA.updateEnableByRegion("LA", 2);
	}

	@Test
	public void testUpdateEnableByRegionWithInvalidEnableValueLesserThanZero() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The enable value has to be 0 (zero) or 1 (one)");

		this.schedulerRepositoryServiceForLA.updateEnableByRegion("LA", -1);
	}
}
