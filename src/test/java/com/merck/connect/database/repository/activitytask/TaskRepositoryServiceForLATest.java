package com.example.connect.database.repository.activitytask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.connect.database.model.activitytask.ActivityTaskRule;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-with-functions-test.xml" })
public class TaskRepositoryServiceForLATest {	
	@Autowired
	@Qualifier("taskRepositoryServiceForLA")
	private TaskRepositoryService taskRepositoryServiceForLA;
	private ActivityTaskRule activityTaskRule;
	
	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void getRuleSuccessfulForLA() {
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST1-ID", "TEST1-COUNTRY", "TEST1-BU");
		assertEquals("TEST1-ID", activityTaskRule.getId());
	
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST3-ID", "TEST3-COUNTRY", "");
		assertEquals("TEST3-ID", activityTaskRule.getId());
		
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST4-ID", null, "");
		assertEquals("TEST4-ID", activityTaskRule.getId());
		
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST1-ID", "", "");
		assertEquals("TEST1-ID", activityTaskRule.getId());
		assertEquals("NA", activityTaskRule.getCountry());
		assertEquals("NA", activityTaskRule.getBusinessUnit());
		assertNull(activityTaskRule.getRegion());
		assertNull(activityTaskRule.getDescription());
		assertEquals("Active", activityTaskRule.getStatus());
		assertTrue(activityTaskRule.isActive());
		assertNull(activityTaskRule.getRule());
		
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST1-ID", "TEST1-COUNTRY", null);
		assertEquals("TEST1-ID", activityTaskRule.getId());
		assertEquals("TEST1-COUNTRY", activityTaskRule.getCountry());
		assertEquals("NA", activityTaskRule.getBusinessUnit());
		assertNull(activityTaskRule.getRegion());
		assertNull(activityTaskRule.getDescription());
		assertEquals("Active", activityTaskRule.getStatus());
		assertTrue(activityTaskRule.isActive());
		assertNull(activityTaskRule.getRule());
	
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST1-ID", "TEST1-COUNTRY", "sarlanga");
		assertEquals("TEST1-ID", activityTaskRule.getId());
		assertEquals("TEST1-COUNTRY", activityTaskRule.getCountry());
		assertEquals("NA", activityTaskRule.getBusinessUnit());
		assertNull(activityTaskRule.getRegion());
		assertNull(activityTaskRule.getDescription());
		assertEquals("Active", activityTaskRule.getStatus());
		assertTrue(activityTaskRule.isActive());
		assertNull(activityTaskRule.getRule());
	
		activityTaskRule = taskRepositoryServiceForLA.getRule("TEST1-ID", "sarlanga", "sarlanga");
		assertEquals("TEST1-ID", activityTaskRule.getId());
		assertEquals("NA", activityTaskRule.getCountry());
		assertEquals("NA", activityTaskRule.getBusinessUnit());
		assertNull(activityTaskRule.getRegion());
		assertNull(activityTaskRule.getDescription());
		assertEquals("Active", activityTaskRule.getStatus());
		assertTrue(activityTaskRule.isActive());
		assertNull(activityTaskRule.getRule());
	}
	
	@Test
	public void getRuleErrorForLA() {
		activityTaskRule = taskRepositoryServiceForLA.getRule(null, null, null);
		assertNull(activityTaskRule);
		
		activityTaskRule = taskRepositoryServiceForLA.getRule("", "", "");
		assertNull(activityTaskRule);
		
		activityTaskRule = taskRepositoryServiceForLA.getRule("","TEST1-ID", null);
		assertNull(activityTaskRule);
	}
	
	@Test
	public void testFindContactByCustomFieldsNotFound() {
		String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		assertTrue(taskRepositoryServiceForLA.findContactByCustomFields(firstName, lastName, medicalId, state, userId).isEmpty());
	}

	@Test
	public void testFindContactByCustomFieldsExistingInDatabase() {
		final String firstName = "Jose David", lastName = "Miranda Echavarria", medicalId = "71774733", state = "", userId = "AGEA-5DBM5Q";
		final Map<String, Object> contactMap = taskRepositoryServiceForLA.findContactByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(54, contactMap.size());
		assertEquals("CO_CST_5430", contactMap.get("EXTID"));
	}

	@Test
	public void testFindAccountByCustomFieldsNotFound() {
		final String accountName = "", colony = "", cnpj = "";
		assertTrue(taskRepositoryServiceForLA.findAccountByCustomFields(accountName, colony, cnpj).isEmpty());
	}

	@Test
	public void testFindAccountByCustomFieldsAccountNameNotFound() {
		final String accountName = "Hospital Aleman", colony = "", cnpj = "";
		assertTrue(taskRepositoryServiceForLA.findAccountByCustomFields(accountName, colony, cnpj).isEmpty());
	}

}
