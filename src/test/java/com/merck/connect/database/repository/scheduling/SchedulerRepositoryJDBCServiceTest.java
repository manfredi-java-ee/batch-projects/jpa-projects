package com.example.connect.database.repository.scheduling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.example.connect.database.model.scheduling.TaskProcess;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class SchedulerRepositoryJDBCServiceTest {
	@Autowired
	@Qualifier(value = "schedulerRepositoryServiceForLA")
	private SchedulerRepositoryService schedulerRepositoryServiceForLA;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testAreAllProcessesFinished() {
		assertEquals(2, schedulerRepositoryServiceForLA.areAllProcessesFinished("HOURLY", "LA"));
	}

	@Test
	public void testAreAllProcessesFinishedNotFound() {
		assertEquals(0, schedulerRepositoryServiceForLA.areAllProcessesFinished("TASK-NAME-NOT-FOUND", "LA"));
	}
	
	@Test
	public void testFindTasksToRestartByRegion() {
		assertEquals(2, schedulerRepositoryServiceForLA.findTasksToRestartByRegion("LA").size());
			
	}

	@Test
	public void disableRestartProcessesWithEmptyTaskProcessList() {
		List<TaskProcess> taskProcesses = Lists.newArrayList();
		assertEquals(0, schedulerRepositoryServiceForLA.disableRestartProcesses(taskProcesses));
	}

	@Test
	public void disableRestartProcesses() {
		final List<TaskProcess> taskProcesses = schedulerRepositoryServiceForLA.findTasksToRestartByRegion("LA");
		assertEquals(2, schedulerRepositoryServiceForLA.disableRestartProcesses(taskProcesses));
		final List<TaskProcess> taskProcessesUpdated = schedulerRepositoryServiceForLA.findTasksToRestartByRegion("LA");
		assertEquals(0, taskProcessesUpdated.size());
	}

	@Test
	public void testFindActiveTasksByName() {
		assertEquals(1, schedulerRepositoryServiceForLA.findActiveTasksByName("DAILY", "LA").size());
	}

	@Test
	public void testFindByTaskNameAndProcessId() {
		assertNotNull(schedulerRepositoryServiceForLA.findByTaskNameAndProcessId("DAILY", "LA",
				"PROCESSID-1"));
	}

	@Test
	public void testUpdateStartTime() {
		final Timestamp startTime = new Timestamp(new Date().getTime());
		schedulerRepositoryServiceForLA.updateStartTime("DAILY", "LA", "PROCESSID-2",
				startTime);
	}

	@Test
	public void testUpdateStartProcessWithNullStartTime() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The task start time cannot be null");
		schedulerRepositoryServiceForLA.updateStartTime("DAILY", "LA", "PROCESSID-3", null);
	}

	@Test
	public void testUpdateStartProcess() {
		assertEquals(1, schedulerRepositoryServiceForLA.updateStartProcess("DAILY", "LA"));
	}

	@Test
	public void testUpdateCompletedProcess() {
		final Timestamp finishTime = new Timestamp(new Date().getTime());
		assertEquals(1, schedulerRepositoryServiceForLA.updateCompletedProcess("DAILY", "LA",
				"PROCESSID-1", finishTime, "00:10:01"));
	}

	@Test
	public void testUpdateCompletedProcessWithNullFinishTime() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The task finish time cannot be null");

		assertEquals(1, schedulerRepositoryServiceForLA.updateCompletedProcess("DAILY", "LA",
				"PROCESSID-5", null, "00:10:01"));
	}

	@Test
	public void testUpdateCompletedProcessWithNullDuration() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The task duration cannot be null");

		final Timestamp finishTime = new Timestamp(new Date().getTime());
		assertEquals(1, schedulerRepositoryServiceForLA.updateCompletedProcess("DAILY", "LA",
				"connect-isales-modification-tracking", finishTime, null));
	}

	@Test
	public void testUpdateErrorProcess() {
		final Timestamp finishTime = new Timestamp(new Date().getTime());
		assertEquals(1, schedulerRepositoryServiceForLA.updateErrorProcess("DAILY", "LA",
				"PROCESSID-1", finishTime, "00:10:01", "error message"));
	}

	@Test
	public void testUpdateErrorProcessWithNullFinishTime() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The task finish time cannot be null");
		assertEquals(1, schedulerRepositoryServiceForLA.updateErrorProcess("DAILY", "LA",
				"connect-isales-modification-tracking", null, "00:10:01", "there is an error message"));
	}

	@Test
	public void testUpdateErrorrocessWithNullDuration() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("The task duration cannot be null");
		final Timestamp finishTime = new Timestamp(new Date().getTime());
		assertEquals(1, schedulerRepositoryServiceForLA.updateErrorProcess("DAILY", "LA",
				"connect-isales-modification-tracking", finishTime, null, "there is an error message"));
	}

	@Test
	public void testUpdateRowsProcessed() {
		assertEquals(1, schedulerRepositoryServiceForLA.updateRowsProcessed("DAILY", "LA",
				"PROCESSID-1", 3, 0));
	}
	
	@Test
	public void testAreProcessesDependent() {
		List<TaskProcess> processes = this.schedulerRepositoryServiceForLA.findActiveTasksByName("NIGHTLY", "LA");
		assertTrue(this.schedulerRepositoryServiceForLA.areProcessesDependent(processes));
	}

	@Test
	public void testAreProcessesDependentWithNo() {
		List<TaskProcess> processes = this.schedulerRepositoryServiceForLA.findActiveTasksByName("NIGHTLY", "LA");
		processes.get(0).setRep("N");
		assertFalse(this.schedulerRepositoryServiceForLA.areProcessesDependent(processes));
	}
	
	
	@Test
	public void testUpdateAllTaskToCompleted() {
	 	assertEquals(1, schedulerRepositoryServiceForLA.updateAllTaskStatusToCompleted());
	}

	
	
	
	
	
	
	
	
	
	
	
	
}
