package com.example.connect.database.repository.activitytask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-mx-test.xml" })
public class TaskRepositoryServiceForMXTest {	
	@Autowired
	@Qualifier("taskRepositoryServiceForMX")
	private TaskRepositoryService taskRepositoryServiceForMX;


	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}
	
	@Test
	public void testFindContactByCustomFields() {
		String firstName = "", lastName = "", medicalId = "", state = "", userId = "";
		assertTrue(taskRepositoryServiceForMX.findContactByCustomFields(firstName, lastName, medicalId, state, userId).isEmpty());
	}

	@Test
	public void testFindByCustomFieldsByMedicalIdAndStateAndLastNameFullMatched() {
		final String firstName = "AGUILAR SANCHEZ", lastName = "GILBERTO ANDRES", medicalId = "", state = "", userId = "";
		final Map<String, Object> contactMap = taskRepositoryServiceForMX.findContactByCustomFields(firstName, lastName, medicalId, state, userId);

		assertEquals(49, contactMap.size());
		assertEquals("MX-0000246632", contactMap.get("EXTID"));
	}

	@Test
	public void testFindByCustomFieldsWithEmptyResultSet() {
		final String accountName = "", colony = "", cnpj = "";
		final Map<String, Object> accountMap = taskRepositoryServiceForMX.findAccountByCustomFields(accountName, colony, cnpj);

		assertEquals(0, accountMap.size());
	}

	@Test
	public void testFindByCustomFieldsByAccountNameAndColonyMatched() {
		final String accountName = "MED TEC 100-TORRE MED TEC 100-CONS 408", colony = "EL CARRIZAL", cnpj = "";
		final Map<String, Object> accountMap = taskRepositoryServiceForMX.findAccountByCustomFields(accountName, colony, cnpj);

		assertEquals(31, accountMap.size());
		assertEquals("MX-0000563133", accountMap.get("EXTID"));
	}
}
