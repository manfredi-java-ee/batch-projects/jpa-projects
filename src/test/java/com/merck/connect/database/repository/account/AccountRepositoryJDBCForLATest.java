package com.example.connect.database.repository.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:connect-database-service-la-test.xml" })
@IntegrationTest
@Transactional(transactionManager = "transactionManagerLA")
@Rollback
public class AccountRepositoryJDBCForLATest {

	@Autowired
	@Qualifier(value = "accountRepositoryJDBCForLA")
	private AccountRepository accountRepositoryForLA;

	@BeforeClass
	public static void setSystemProps() {
	    System.setProperty("spring.profiles.active", "development");
	}

	@Test
	public void testGetAccountQuery() {
		assertEquals("NA", accountRepositoryForLA.getAccountQuery());
	}

	@Test
	public void testFindByCustomFields() {
		final String accountName = "", colony = "", cnpj = "";
		final List<Map<String, Object>>  contactList = accountRepositoryForLA.findByCustomFields(accountName, colony, cnpj);

		assertTrue(contactList.isEmpty());
	}

}
