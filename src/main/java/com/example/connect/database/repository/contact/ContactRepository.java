package com.example.connect.database.repository.contact;

import java.util.List;
import java.util.Map;

public interface ContactRepository {

	String getContactQuery();

	List<Map<String, Object>> findByCustomFields(
			final String firstName, final String lastName, final String medicalId, final String state, final String userId);

}
