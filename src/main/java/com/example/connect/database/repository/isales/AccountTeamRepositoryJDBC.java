package com.example.connect.database.repository.isales;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.isales.AccountTeam;

public class AccountTeamRepositoryJDBC extends JdbcDaoSupport implements AccountTeamRepository {

	public AccountTeamRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	private static class AccountTeamRowMapper implements RowMapper<AccountTeam> {
		@Override
		public AccountTeam mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final  AccountTeam  accountTeam = new  AccountTeam();
			accountTeam.setId(rs.getString("ID"));
			accountTeam.setUserId(rs.getString("userid"));
			accountTeam.setAccountid(rs.getString("accountid"));
			accountTeam.setAction(rs.getString("action"));

			return accountTeam;
		}
	}
		
	@Override
	public List<AccountTeam> findByAction(String action) {
		return getJdbcTemplate().query("select ID, userid, accountid, action from auto_at_isales_proc_vw where action = ?", 
				new String[] { action }, 
				new AccountTeamRowMapper());
	}
}