package com.example.connect.database.repository.account;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;


public class AccountRepositoryJDBCForMX extends JdbcDaoSupport implements AccountRepository {
	private String accountQuery;

	public AccountRepositoryJDBCForMX(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> findByCustomFields(String accountName, String colony, String cnpj) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM ACCOUNTS ");
	    sql.append("WHERE EXTID IS NOT NULL ");
	    sql.append("AND EXTID LIKE 'MX-%' ");
		sql.append("AND translate(upper(accountname),'ÁÉÍÓÚÑ','AEIOUN') = translate(upper(?),'ÁÉÍÓÚÑ','AEIOUN') ");
   		sql.append("AND ( translate(upper(colony),'ÁÉÍÓÚÑ','AEIOUN') = translate(upper(?),'ÁÉÍÓÚÑ','AEIOUN') ");
   		sql.append("OR COLONY IS NULL ");
   		sql.append("AND ? IS NULL ) ");

   		return getJdbcTemplate().queryForList(sql.toString(), accountName, colony, colony);
   	}

	public String getAccountQuery() {
		return accountQuery;
	}

	public void setAccountQuery(final String accountQuery) {
		this.accountQuery = accountQuery;
	}

}
