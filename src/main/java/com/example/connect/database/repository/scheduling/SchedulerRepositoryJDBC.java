package com.example.connect.database.repository.scheduling;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.scheduling.ProcessType;
import com.example.connect.database.model.scheduling.TaskActive;
import com.example.connect.database.model.scheduling.TaskProcess;
import com.example.connect.database.model.scheduling.TaskReRunning;
import com.example.connect.database.model.scheduling.TaskStatus;

public class SchedulerRepositoryJDBC extends JdbcDaoSupport implements SchedulerRepository {
	private static final String BASE_QUERY_APP_TASK_SCHEDULER = "SELECT TASK_NAME, REGION, PROCESS_TYPE, PROCESS_ID, "
			+ "TASK_STATUS, LOCATION, APPLICATION_NAME, ARGUMENTS, REP, SEQUENTIAL_ORDER, ROWS_PROCESSED, ROWS_DELETED, "
			+ "DURATION, START_TIME, FINISH_TIME, ACTIVE, ERROR_MESSAGE, DESCRIPTION, RE_RUNNING "
			+ "FROM APP_TASK_SCHEDULER";

	public SchedulerRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TaskProcess> areAllProcessesFinished(final String taskName, final String region) {
		final StringBuilder sb = new StringBuilder(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" WHERE TASK_NAME = ? AND REGION = ? AND ACTIVE = ? ")
				.append(" MINUS ")
				.append(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" WHERE TASK_NAME = ? AND REGION = ? AND ACTIVE = ? AND TASK_STATUS != ? AND TASK_STATUS != ? ");
		return getJdbcTemplate().query(
				sb.toString(), 
				new String[] { 
						taskName, region, TaskActive.Y.name(), 
						taskName, region, TaskActive.Y.name(), TaskStatus.IN_PROGRESS.name(), TaskStatus.RESTART.name() }, 
				new TaskProcessRowMapper());
	}

	@Override
	@Transactional(readOnly = true)
	public List<TaskProcess> findTasksToRestartByRegion(final String region, final TaskActive active) {
		final StringBuilder sb = new StringBuilder(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" WHERE REGION = ? AND ACTIVE = ? AND TASK_STATUS = ? ")
				.append(" ORDER BY TASK_NAME, SEQUENTIAL_ORDER ASC ");
		return getJdbcTemplate().query(
				sb.toString(), 
				new String[] { region, active.name(), TaskStatus.RESTART.name() }, 
				new TaskProcessRowMapper());
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int disableRestartProcess(final TaskProcess taskProcess) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET TASK_STATUS = ? "
				+ "WHERE TASK_NAME = ? AND REGION = ? AND PROCESS_ID = ? AND TASK_STATUS = ? ", 
				new Object[] { TaskStatus.IN_PROGRESS.name(), taskProcess.getTaskName(), taskProcess.getRegion(),
						taskProcess.getProcessId(), TaskStatus.RESTART.name() });
	}

	@Override
	@Transactional(readOnly = true)
	public List<TaskProcess> findByTaskName(final String taskName, final String region, final TaskActive active) {
		final StringBuilder sb = new StringBuilder(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" WHERE TASK_NAME = ? AND REGION = ? AND ACTIVE = ? AND TASK_STATUS != ? ")
				.append(" ORDER BY SEQUENTIAL_ORDER ASC");
		return getJdbcTemplate().query(
				sb.toString(),
				new String[] { taskName, region, active.name(), TaskStatus.RESTART.name() },
				new TaskProcessRowMapper());
	}

	@Override
	@Transactional(readOnly = true)
	public TaskProcess findByTaskNameAndProcessId(final String taskName, final String region, final String processId) {
		final StringBuilder sb = new StringBuilder(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" where TASK_NAME = ? AND REGION = ? AND PROCESS_ID = ? ")
				.append(" ORDER BY SEQUENTIAL_ORDER ASC");
		final List<TaskProcess> list = getJdbcTemplate().query(sb.toString(), 
				new String[] { taskName, region,processId   }, 
				new TaskProcessRowMapper());

		return list.isEmpty() ? null : list.get(0);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateActiveTasksToInProcess(final String taskName, final String region) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET TASK_STATUS = ? "
				+ "WHERE TASK_NAME = ? AND REGION = ? AND ACTIVE = ? ", 
				new Object[] { TaskStatus.IN_PROGRESS.name(), taskName, region, TaskActive.Y.name() });
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateStartTime(final String taskName, final String region, final String processId, final Timestamp start) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET START_TIME = ? "
				+ "WHERE TASK_NAME = ? AND REGION = ? AND PROCESS_ID = ? ", 
				new Object[] { start, taskName, region, processId });
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateEndProcess(final String taskName, final String region, 
			final String processId, final TaskStatus status, final Timestamp finish, 
			final String duration, final String errorMessage) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET FINISH_TIME = ?, TASK_STATUS = ?, DURATION = ?, ERROR_MESSAGE = ? "
				+ "WHERE TASK_NAME = ? AND REGION = ? AND PROCESS_ID = ? ", 
				new Object[] { finish, status.name(), duration,	errorMessage, taskName, region, processId }); 
	}
 
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updatePostLoadProcessError(final String taskName, final String region,final String errorMessage) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER SET"
				+ " TASK_STATUS = ?, ERROR_MESSAGE = ? "
				+ " WHERE TASK_NAME = ? AND REGION = ? AND REP = 'P' ", 
				new Object[] { TaskStatus.ERROR.name(), errorMessage,taskName, region }); 
	}
	
	  
	
	
	@Override
	public int updateRowsProcessed(final String taskName, final String region,
			final String processId, final int rowsProcessed, final int rowsDeleted) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET ROWS_PROCESSED = ?, ROWS_DELETED = ? "
				+ "WHERE TASK_NAME = ? AND REGION = ? AND PROCESS_ID = ? ", 
				new Object[] { rowsProcessed, rowsDeleted, taskName, region, processId });
	}
	
	private static class TaskProcessRowMapper implements RowMapper<TaskProcess> {
		@Override
		public TaskProcess mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final TaskProcess taskProcess = new TaskProcess();
			taskProcess.setTaskName(rs.getString("TASK_NAME"));
			taskProcess.setRegion(rs.getString("REGION"));
			taskProcess.setType(ProcessType.valueOf(rs.getString("PROCESS_TYPE")));
			taskProcess.setProcessId(rs.getString("PROCESS_ID"));
			taskProcess.setStatus(TaskStatus.valueOf(rs.getString("TASK_STATUS")));
			taskProcess.setLocation(rs.getString("LOCATION"));
			taskProcess.setApplicationName(rs.getString("APPLICATION_NAME"));
			taskProcess.setArguments(rs.getString("ARGUMENTS"));
			taskProcess.setRep(rs.getString("REP"));
			taskProcess.setSequentialOrder(rs.getInt("SEQUENTIAL_ORDER"));
			taskProcess.setRowsProcessed(rs.getInt("ROWS_PROCESSED"));
			taskProcess.setRowsDeleted(rs.getInt("ROWS_DELETED"));
			taskProcess.setDuration(rs.getString("DURATION"));
			taskProcess.setStart(rs.getTimestamp("START_TIME"));
			taskProcess.setEnd(rs.getTimestamp("FINISH_TIME"));
			taskProcess.setActive(TaskActive.valueOf(rs.getString("ACTIVE")));
			taskProcess.setErrorMessage(rs.getString("ERROR_MESSAGE"));
			taskProcess.setDescription(rs.getString("DESCRIPTION"));
			taskProcess.setRerunning(TaskReRunning.valueOf(rs.getString("RE_RUNNING")));

			return taskProcess;
		}
	}
	
	
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateAllTaskStatus(TaskStatus TaskStatus ) {
		return getJdbcTemplate().update("UPDATE APP_TASK_SCHEDULER "
				+ "SET  TASK_STATUS = ?  WHERE TASK_STATUS = 'IN_PROGRESS' ", 
				new Object[] { TaskStatus.toString() });
	}

	@Override
	public int checkDepencenciesError(final String taskName,final String region,final String procId) {
  
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(*) ");
	    sql.append("FROM APP_TASK_SCHEDULER P ");
	    sql.append("INNER JOIN APP_TASK_DEPENDECIES D ON P.REGION = D.REGION AND P.PROCESS_ID = D.PROC_ID ");
	    sql.append("INNER JOIN APP_TASK_SCHEDULER PD ON PD.REGION = P.REGION AND PD.PROCESS_ID = D.PROC_DEP ");
	    sql.append("WHERE P.REGION = ? AND P.PROCESS_ID = ? AND D.ACTIVE = 'Y' AND PD.TASK_STATUS = ?  AND P.TASK_NAME = ? AND P.ACTIVE='Y' ");
	 
		return getJdbcTemplate().queryForObject(sql.toString(),
				new Object[] { region, procId,  TaskStatus.ERROR.name(),taskName },
				Integer.class);
   		 
	}
	
	
	
	
	

}
