package com.example.connect.database.repository.account;

import java.util.List;
import java.util.Map;

public interface AccountRepository {

	String getAccountQuery();

	List<Map<String, Object>> findByCustomFields(
			final String accountName, final String colony, final String cnpj);

}
