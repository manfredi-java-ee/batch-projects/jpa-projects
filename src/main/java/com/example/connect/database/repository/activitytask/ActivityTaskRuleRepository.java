package com.example.connect.database.repository.activitytask;

import java.util.List;
import java.util.Map;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.activitytask.ActivityTaskRule;

@NoRepositoryBean
public interface ActivityTaskRuleRepository {

	
	List<ActivityTaskRule> getBusinessRule();

	List<Map<String, Object>> executeQuery(final String sql, final Map<String, String> param);
	
}
