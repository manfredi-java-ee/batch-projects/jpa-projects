package com.example.connect.database.repository.alert;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AlertRepository {
		
		boolean runAlert(String schema, String alertId, String alertGrpId,boolean logExecutionDate );


}
