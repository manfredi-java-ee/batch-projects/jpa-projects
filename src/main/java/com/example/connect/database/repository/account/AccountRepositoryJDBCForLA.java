package com.example.connect.database.repository.account;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.google.common.collect.Lists;


public class AccountRepositoryJDBCForLA extends JdbcDaoSupport implements AccountRepository {

	private String accountQuery;

	public AccountRepositoryJDBCForLA(final DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@Override
	public List<Map<String, Object>> findByCustomFields(final String accountName, final String colony, final String cnpj) {
		return Lists.newArrayList();
	}

	@Override
	public String getAccountQuery() {
		return accountQuery;
	}

	public void setAccountQuery(final String accountQuery) {
		this.accountQuery = accountQuery;
	}

}
