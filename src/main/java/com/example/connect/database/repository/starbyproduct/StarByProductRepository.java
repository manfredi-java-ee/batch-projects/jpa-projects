package com.example.connect.database.repository.starbyproduct;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StarByProductRepository {

	boolean isAudit(final String userId, final String contactId);

}
