package com.example.connect.database.repository.scheduling;

import com.example.connect.database.model.scheduling.WebServiceMonitor;


public interface WebServiceMonitorRepositoryService {
	WebServiceMonitor findByWsName(String wsName);
	void setWebServiceMonitorRepository(WebServiceMonitorRepository webServiceMonitorRepository);
}