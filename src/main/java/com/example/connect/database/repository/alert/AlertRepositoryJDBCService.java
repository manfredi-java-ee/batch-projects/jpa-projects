package com.example.connect.database.repository.alert;

import org.springframework.transaction.annotation.Transactional;


public class AlertRepositoryJDBCService implements AlertRepositoryService {
	


	private AlertRepository alertRepository;
	
	
	@Transactional(readOnly = true)
	public  boolean runAlert(String schema, String alertId, String alertGrpId,boolean logExecutionDate ){
		return this.alertRepository.runAlert(schema,alertId,alertGrpId, logExecutionDate);
	}

	 
	 

	
	public void setAlertRepository(AlertRepository alertRepository) {
		this.alertRepository = alertRepository;
	}

	 
 
}