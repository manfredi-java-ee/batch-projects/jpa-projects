package com.example.connect.database.repository.multilanguaje;

 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.languaje.Multilanguaje;
import com.example.connect.database.model.languaje.MultilanguajeType;
 
public class MultilanguajeRepositoryJDBC extends JdbcDaoSupport implements MultilanguajeRepository {
	
 
	public MultilanguajeRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	
	
	private static class MultilanguajeRowMapper implements RowMapper<Multilanguaje> {
		@Override
		public Multilanguaje mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final Multilanguaje multi = new Multilanguaje();
			
			multi.setId(rs.getString("ID"));
			multi.setLanguaje(rs.getString("LANGUAJE"));
			multi.setTraslation(rs.getString("TRANSLATION"));
			multi.setType(rs.getString("TYPE"));
			multi.setCountry(rs.getString("COUNTRY"));
	 
			return multi;
		}
	}
 
	
	public List<Multilanguaje> getTranslationForType(MultilanguajeType type) {
		
		final StringBuilder sql = new StringBuilder();
		sql.append(" SELECT MT.ID,MT.LANGUAJE,MT.TRANSLATION,MT.TYPE,MC.COUNTRY ");
	    sql.append(" FROM MULTILANGUAJE_TRASLATION MT, MULTILANGUAJE_COUNTRY MC  ");
	    sql.append(" WHERE MT.LANGUAJE = MC.LANGUAJE AND MT.TYPE = ?  ");
		 
		return getJdbcTemplate().query(sql.toString(), 
				new String[] { type.toString() }, 
				new MultilanguajeRowMapper());

	}


 
	

}