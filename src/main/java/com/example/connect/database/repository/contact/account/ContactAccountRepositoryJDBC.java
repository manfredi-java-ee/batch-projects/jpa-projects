package com.example.connect.database.repository.contact.account;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ContactAccountRepositoryJDBC extends JdbcDaoSupport implements ContactAccountRepository {

 
	public ContactAccountRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> getAccountAssociatedToContactAndBeInPanel( final String contactId, final String userId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM CONTACTS_X_ACCOUNT CA  ");
	    sql.append("WHERE CA.CONTACTID  = ? ");
	    sql.append("AND CA.ACCOUNTID in ");
	    sql.append("( SELECT AE.ACCOUNTID    ");
	    sql.append("  FROM ACCOUNTS_X_EMP AE ");
	    sql.append("  WHERE AE.USERID = ?  )");
   
   		return getJdbcTemplate().queryForList(sql.toString(), contactId, userId);
	}
	
	@Override
	public List<Map<String, Object>> getAccountAssociatedToOtherContact( final String contactId,final String accountId, final String userId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM CONTACTS_X_ACCOUNT CA  ");
	    sql.append("WHERE CA.ACCOUNTID = ?  ");
	    sql.append("AND CA.CONTACTID IN ( ");
	    sql.append("SELECT CE.CONTACTID ");
	    sql.append("FROM CONTACTS_X_EMP CE    ");
	    sql.append("WHERE CE.USERID = ? ");
	    sql.append("AND ce.CONTACTID != ? )");
   
   		return getJdbcTemplate().queryForList(sql.toString(),accountId,userId, contactId);
	}
	
 

 

}
