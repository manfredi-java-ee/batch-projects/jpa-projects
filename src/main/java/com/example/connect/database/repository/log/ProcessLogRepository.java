package com.example.connect.database.repository.log;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ProcessLogRepository {

	int log(final String processId, final String type, final String description, final String message);

}