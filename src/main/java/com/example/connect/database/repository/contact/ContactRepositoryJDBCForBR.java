package com.example.connect.database.repository.contact;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ContactRepositoryJDBCForBR extends JdbcDaoSupport implements ContactRepository {

	private String contactQuery;

	public ContactRepositoryJDBCForBR(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> findByCustomFields(String firstName, String lastName, String medicalId,
			String state, String userId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT C.*, CS.* ");
	    sql.append("FROM CONTACTS C LEFT JOIN CONTACTS_SPECIALTIES CS ON C.CONTACTID = CS.CONTACTID ");
	    sql.append("WHERE C.EXTID IS NOT NULL ");
	    sql.append("AND C.EXTID LIKE 'BR-%' ");
	    sql.append("AND CS.SPECIALTYORDER = 1 ");
	    sql.append("AND C.LOCALREF = ? ");
	    sql.append("AND upper(C.STATE) = upper(?) ");
   		sql.append("AND translate(upper(C.contactlastname),'ÁÉÍÓÚÑ','AEIOUN') = translate(upper(?),'ÁÉÍÓÚÑ','AEIOUN') ");
	    sql.append("AND C.localref != '0000000' AND C.localref != '000000' ");

   		return getJdbcTemplate().queryForList(sql.toString(), medicalId, state, lastName);
	}

	@Override
	public String getContactQuery() {
		return contactQuery;
	}

	public void setContactQuery(String contactQuery) {
		this.contactQuery = contactQuery;
	}

}
