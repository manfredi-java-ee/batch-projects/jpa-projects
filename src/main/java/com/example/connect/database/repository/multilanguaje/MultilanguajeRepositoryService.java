package com.example.connect.database.repository.multilanguaje;

import java.util.List;

import com.example.connect.database.model.languaje.Multilanguaje;



public interface MultilanguajeRepositoryService {
	
	Multilanguaje getTraslationMessagesById( final String id, final String country) ;

	String getTraslationFields(final List<String> fieldsToTraslate ,final String country) ;
	 

}