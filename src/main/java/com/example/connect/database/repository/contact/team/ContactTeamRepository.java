package com.example.connect.database.repository.contact.team;

import java.util.List;
import java.util.Map;

public interface ContactTeamRepository {

	List<Map<String, Object>> foundContactInOtherPanelByDifferentSalesTeam(final String contactExternalId,final String userId, final List<String> salesTeamInClause );

	List<Map<String, Object>> foundContactInOtherPanelBySameSalesTeam(final String contactExternalId,final String userId, final String salesTeamId );

	
	
}
