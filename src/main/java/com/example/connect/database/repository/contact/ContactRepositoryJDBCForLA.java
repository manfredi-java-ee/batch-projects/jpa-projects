package com.example.connect.database.repository.contact;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ContactRepositoryJDBCForLA extends JdbcDaoSupport implements ContactRepository {

	private String contactQuery;

	public ContactRepositoryJDBCForLA(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> findByCustomFields(final String firstName, final String lastName, final String medicalId,
			final String state, final String userId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM CONTACTS ");
	    sql.append("WHERE EXTID IS NOT NULL ");
   		sql.append("AND UPPER(LOCALREF) = UPPER(?) ");
   		sql.append("AND UTL_MATCH.JARO_WINKLER_SIMILARITY(translate(UPPER(contactfirstname),'ÁÉÍÓÚÑ','AEIOUN'), UPPER(?)) > 40");
   		sql.append("AND UTL_MATCH.JARO_WINKLER_SIMILARITY(translate(UPPER(contactlastname),'ÁÉÍÓÚÑ','AEIOUN'), upper(?)) > 90");
   		sql.append("AND UPPER(primary_country) in (select upper(country) from employees where userid = ?) ");

		return getJdbcTemplate().queryForList(sql.toString(), medicalId, firstName, lastName, userId);
	}

	@Override
	public String getContactQuery() {
		return contactQuery;
	}

	public void setContactQuery(String contactQuery) {
		this.contactQuery = contactQuery;
	}

}
