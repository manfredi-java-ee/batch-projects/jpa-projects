package com.example.connect.database.repository.scheduling;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.scheduling.WebServiceMonitor;

@NoRepositoryBean
public interface WebServiceMonitorRepository {
	WebServiceMonitor findByWsName(String wsName);
}
