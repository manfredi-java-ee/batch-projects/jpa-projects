package com.example.connect.database.repository.log;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TaskLogRepository {

	int log(final String name, final String objectId,final String logType,final String logMessage,final String adminStatus,final String mgrAprovStatus, final String status);

}