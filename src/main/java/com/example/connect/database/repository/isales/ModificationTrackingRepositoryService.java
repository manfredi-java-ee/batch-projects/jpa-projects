package com.example.connect.database.repository.isales;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.example.connect.database.model.isales.AccountTeam;
import com.example.connect.database.model.isales.Allocation;
import com.example.connect.database.model.isales.ContactTeam;
import com.example.connect.database.repository.log.ProcessLogRepository;


public class ModificationTrackingRepositoryService {
	private AllocationRepository allocationRepository;
	private ContactTeamRepository contactTeamRepository;
	private ProcessLogRepository processLogRepository;
	

	private AccountTeamRepository accountTeamRepository;
	
	
	
	@Transactional(readOnly = true)
	public List<Allocation> findAllocationByAction(final String action) {
		if (StringUtils.isEmpty(action)) {
			return Lists.newArrayList();
		} else {
			return allocationRepository.findByAction(action);	
		}
	}

	@Transactional(readOnly = true)
	public List<ContactTeam> findContactTeamByAction(String action) {
		if (StringUtils.isEmpty(action)) {
			return Lists.newArrayList();
		} else {
			return contactTeamRepository.findByAction(action);	
		}
	}
	
	
	
	@Transactional(readOnly = true)
	public List<AccountTeam> findAccountTeamByAction(String action) {
		if (StringUtils.isEmpty(action)) {
			return Lists.newArrayList();
		} else {
			return accountTeamRepository.findByAction(action);	
		}
	}
	
	
	
	
	

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int log(final String processId, final String type, final String description, final String message) {
		return processLogRepository.log(processId, type, description, message);
	}

	public void setAllocationRepository(AllocationRepository allocationRepository) {
		this.allocationRepository = allocationRepository;
	}

	public void setContactTeamRepository(ContactTeamRepository contactTeamRepository) {
		this.contactTeamRepository = contactTeamRepository;
	}

	public void setProcessLogRepository(ProcessLogRepository processLogRepository) {
		this.processLogRepository = processLogRepository;
	}
	
	
 
	public void setAccountTeamRepository(AccountTeamRepository accountTeamRepository) {
		this.accountTeamRepository = accountTeamRepository;
	}

}