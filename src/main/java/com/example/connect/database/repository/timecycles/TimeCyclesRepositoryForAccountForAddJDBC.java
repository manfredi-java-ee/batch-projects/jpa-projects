package com.example.connect.database.repository.timecycles;

 
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class TimeCyclesRepositoryForAccountForAddJDBC extends JdbcDaoSupport implements TimeCyclesRepository {

	private static final String QUERY_WINDOWS_FOR_ADD_ACCOUNT = " SELECT COUNTRY, NVL(BUSINESS_UNIT, 'NA') BUSINESS_UNIT, "
			+ " CYCLEYEAR, CYCLEID, CYCLESTART, CYCLEEND, CRE_STA_DT, CRE_END_DT, CRE_APP_DT, CRE_STA_DT_ACC, CRE_END_DT_ACC,"
			+ " CRE_APP_DT_ACC "
			+ " FROM TIMECYCLES "
			+ " WHERE trunc(sysdate) >= CRE_STA_DT_ACC AND trunc(sysdate) <= CRE_END_DT_ACC ";


	public TimeCyclesRepositoryForAccountForAddJDBC(final DataSource dataSource) {
		setDataSource(dataSource);
	}
 
	@Override
	public List<Map<String, Object>> getWindows() {
		return getJdbcTemplate().queryForList(QUERY_WINDOWS_FOR_ADD_ACCOUNT);
	}

}
