package com.example.connect.database.repository.starbyproduct;

public interface StarByProductRepositoryService {

	boolean isAudit(final String userId, final String contactId);

}
