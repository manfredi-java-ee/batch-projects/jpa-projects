package com.example.connect.database.repository.contact.account;

import java.util.List;
import java.util.Map;

public interface ContactAccountRepository {

 
	List<Map<String, Object>> getAccountAssociatedToContactAndBeInPanel(final String contactId, final String userId);

	List<Map<String, Object>> getAccountAssociatedToOtherContact(final String contactId,final String accountId, final String userId);

}
