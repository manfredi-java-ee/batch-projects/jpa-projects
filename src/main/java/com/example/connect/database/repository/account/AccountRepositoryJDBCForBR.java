package com.example.connect.database.repository.account;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class AccountRepositoryJDBCForBR extends JdbcDaoSupport implements AccountRepository {

	private String accountQuery;
	
	public AccountRepositoryJDBCForBR(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> findByCustomFields(final String accountName, final String colony, final String cnpj) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM ACCOUNTS ");
	    sql.append("WHERE EXTID IS NOT NULL ");
	    sql.append("AND EXTID LIKE 'BR-%' ");
		sql.append("AND upper(cnpj) = upper(?) ");

   		return getJdbcTemplate().queryForList(sql.toString(), cnpj);
   	}

	public String getAccountQuery() {
		return accountQuery;
	}

	public void setAccountQuery(final String accountQuery) {
		this.accountQuery = accountQuery;
	}

}
