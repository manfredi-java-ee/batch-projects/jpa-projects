package com.example.connect.database.repository.scheduling;

public interface DashboardMonitorRepositoryService {
	int updateEnableByRegion(String region, int enable);
	void setDashboardMonitorRepository(DashboardMonitorRepository dashboardMonitorRepository);
}