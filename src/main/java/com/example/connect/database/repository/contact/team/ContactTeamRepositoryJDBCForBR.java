package com.example.connect.database.repository.contact.team;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ContactTeamRepositoryJDBCForBR extends JdbcDaoSupport implements ContactTeamRepository {
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public ContactTeamRepositoryJDBCForBR(final DataSource dataSource) {
	    this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
 		 setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> foundContactInOtherPanelByDifferentSalesTeam(
			final String contactExternalId, final String userId, final List<String> salesTeamInClause) {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT CE.* , E.SALESTEAMID, E.EXTID, E.USERFIRSTNAME, E.USERLASTNAME ");
	    sql.append("FROM CONTACTS_X_EMP CE  ");
	    sql.append("INNER JOIN EMPLOYEES E ON CE.USERID = E.USERID ");
	    sql.append("INNER JOIN CONTACTS C ON CE.CONTACTID = C.CONTACTID ");
        sql.append("WHERE C.EXTID = :contactExternalId ");
        sql.append("AND E.USERID != :userId  ");
	    sql.append("AND E.SALESTEAMID IN (:salesTeamId) ");
	    sql.append("AND (SELECT SALESTEAMID FROM EMPLOYEES WHERE USERID = :userId) != E.SALESTEAMID ");

		final SqlParameterSource namedParameters = new MapSqlParameterSource("contactExternalId", contactExternalId)
															 .addValue("userId", userId)
															 .addValue("salesTeamId", salesTeamInClause); 

		return namedParameterJdbcTemplate.queryForList(sql.toString(), namedParameters);
	}


	@Override
	public List<Map<String, Object>> foundContactInOtherPanelBySameSalesTeam(
			final String contactExternalId, final String userId, final String salesTeamId) {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT CE.* , E.SALESTEAMID, E.EXTID, E.USERFIRSTNAME, E.USERLASTNAME ");
	    sql.append("FROM CONTACTS_X_EMP CE  ");
	    sql.append("INNER JOIN EMPLOYEES E ON CE.USERID = E.USERID ");
	    sql.append("INNER JOIN CONTACTS C ON CE.CONTACTID = C.CONTACTID ");
	    sql.append("WHERE C.EXTID = ? ");
	    sql.append("AND E.USERID !=  ?  "); 
	    sql.append("AND E.SALESTEAMID = (?) ");
   	
	    return getJdbcTemplate().queryForList(
	    		sql.toString(), 
	    		contactExternalId,
	    		userId,
	    		salesTeamId);
	}

}
