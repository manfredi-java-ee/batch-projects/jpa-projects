package com.example.connect.database.repository.dataloader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.dataloader.ParameterLoaderDB;
import com.example.connect.database.repository.log.ProcessLogRepository;

public class DataLoaderRepositoryService {

	private DataLoaderRepository dataLoaderRepositoryJDBC;
	private ProcessLogRepository processLogRepository;

	@Transactional(readOnly = true)
	public ResultSet findExtraction(final String sql, final String region, final String action) throws SQLException {
		return dataLoaderRepositoryJDBC.findExtraction(sql, region, action);
	}
	
	@Transactional(readOnly = true)
	public int infoRowProcess(final String id, final int row, final String link, final String status) throws SQLException {
		return dataLoaderRepositoryJDBC.infoRowProcess(id, row, link, status);
	}
	
	@Transactional(readOnly = true)
	public List<ParameterLoaderDB> findParameter() throws SQLException {
		return dataLoaderRepositoryJDBC.findParameter();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int log(final String processId, final String type, final String description, final String message) {
		return processLogRepository.log(processId, type, description, message);
	}

	public void setProcessLogRepository(
			ProcessLogRepository processLogRepository) {
		this.processLogRepository = processLogRepository;
	}

	public ProcessLogRepository getProcessLogRepository() {
		return processLogRepository;
	}

	public DataLoaderRepository getDataLoaderRepositoryJDBC() {
		return dataLoaderRepositoryJDBC;
	}

	public void setDataLoaderRepositoryJDBC(
			DataLoaderRepository dataLoaderRepositoryJDBC) {
		this.dataLoaderRepositoryJDBC = dataLoaderRepositoryJDBC;
	}
}