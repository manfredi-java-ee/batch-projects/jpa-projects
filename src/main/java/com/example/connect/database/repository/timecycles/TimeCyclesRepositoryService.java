package com.example.connect.database.repository.timecycles;

import java.util.List;
import java.util.Map;

public interface TimeCyclesRepositoryService {

	List<Map<String, Object>> getWindowsAvailable(String country, String businessUnit);

}