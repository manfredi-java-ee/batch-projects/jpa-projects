package com.example.connect.database.repository.alert;



public interface AlertRepositoryService {
 
	boolean runAlert(String schema, String alertId, String alertGrpId,boolean logExecutionDate);

}