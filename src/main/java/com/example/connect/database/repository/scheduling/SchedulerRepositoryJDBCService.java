package com.example.connect.database.repository.scheduling;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.example.connect.database.model.scheduling.TaskActive;
import com.example.connect.database.model.scheduling.TaskProcess;
import com.example.connect.database.model.scheduling.TaskStatus;


public class SchedulerRepositoryJDBCService implements SchedulerRepositoryService {
	private SchedulerRepository schedulerRepository;

	@Override
	@Transactional(readOnly = true)
	public int areAllProcessesFinished(final String taskName, final String region) {
		return this.schedulerRepository.areAllProcessesFinished(taskName, region).size();
	}

	@Override
	@Transactional(readOnly = true)
	public List<TaskProcess> findTasksToRestartByRegion(final String region) {
		return this.schedulerRepository.findTasksToRestartByRegion(region, TaskActive.Y);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int disableRestartProcesses(final List<TaskProcess> taskProcesses) {
		int count = 0;
		for (final TaskProcess taskProcess : taskProcesses) {
			count += this.schedulerRepository.disableRestartProcess(taskProcess);
		}
		return count;
	}

	@Transactional(readOnly = true)
	public List<TaskProcess> findActiveTasksByName(final String taskName, final String region) {
		return this.schedulerRepository.findByTaskName(taskName, region, TaskActive.Y);
	}

	@Transactional(readOnly = true)
	public TaskProcess findByTaskNameAndProcessId(
			final String taskName, final String region, final String ProcessId) {
		return this.schedulerRepository.findByTaskNameAndProcessId(taskName, region, ProcessId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateStartProcess(String taskName, String region) {
		return this.schedulerRepository.updateActiveTasksToInProcess(taskName, region);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateStartTime(String taskName, String region, String processId, Timestamp start) {
		Assert.notNull(start, "The task start time cannot be null");
		return this.schedulerRepository.updateStartTime(taskName, region, processId, start);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateCompletedProcess(String taskName, String region, String processId, Timestamp finish, 
			String duration) {
		Assert.notNull(finish, "The task finish time cannot be null");
		Assert.notNull(duration, "The task duration cannot be null");
		return this.schedulerRepository.updateEndProcess(
				taskName, region, processId, TaskStatus.COMPLETED, finish, duration, null);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateErrorProcess(String taskName, String region, String processId, Timestamp finish,
			String duration, String errorMessage) {
		Assert.notNull(finish, "The task finish time cannot be null");
		Assert.notNull(duration, "The task duration cannot be null");
		return this.schedulerRepository.updateEndProcess(
				taskName, region, processId, TaskStatus.ERROR, finish, duration, errorMessage);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateErrorDependenceProcess(String taskName, String region, String processId, Timestamp finish,
			String duration, String errorMessage) {
		Assert.notNull(finish, "The task finish time cannot be null");
		Assert.notNull(duration, "The task duration cannot be null");
		return this.schedulerRepository.updateEndProcess(
				taskName, region, processId, TaskStatus.ERROR_DEPENDENCIES, finish, duration, errorMessage);
	}

	
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateAllTaskStatusToCompleted() {
	 	return this.schedulerRepository.updateAllTaskStatus(TaskStatus.COMPLETED);
	}

	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean checkDepencenciesError(String taskName, String region, String procId) {

		int count = this.schedulerRepository.checkDepencenciesError(taskName, region, procId);
		
		if (count == 0) {

			return true;

		} else {

			return false;
		}
			
		
	}
	
	
	
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateRowsProcessed(String taskName, String region, String applicationName, int rowsProcessed,
			int rowsDeleted) {
		return this.schedulerRepository.updateRowsProcessed(
				taskName, region, applicationName, rowsProcessed, rowsDeleted);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
 	public int updateErrorPostLoadProcess(String taskName, String region, String errorMessage) {
 
		return this.schedulerRepository.updatePostLoadProcessError(taskName, region, errorMessage) ;
	 
	}
	
	
	 
	
	
	
	
	public boolean areProcessesDependent(final List<TaskProcess> processes) {
		for (final TaskProcess taskProcess : processes) {
			if (!("Y".equalsIgnoreCase(taskProcess.getRep()))) {
				return false;
			}
		}
		return true;
	}
	
	
	public boolean existProcessesDependent(final List<TaskProcess> processes) {
		for (final TaskProcess taskProcess : processes) {
			if (("Y".equalsIgnoreCase(taskProcess.getRep()))) {
				return true;
			}
		}
		return false;
	}
	
	
	public boolean existProcessesReRunning(final List<TaskProcess> processes) {
		for (final TaskProcess taskProcess : processes) {
			if ("Y".equalsIgnoreCase(taskProcess.getRerunning().name())) {
				return true;
			}
		}
		return false;
	}
	
	

	public void setSchedulerRepository(SchedulerRepository schedulerRepository) {
		this.schedulerRepository = schedulerRepository;
	}

	 


}
