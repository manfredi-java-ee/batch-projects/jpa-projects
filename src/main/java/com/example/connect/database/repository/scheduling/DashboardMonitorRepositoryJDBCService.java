package com.example.connect.database.repository.scheduling;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


public class DashboardMonitorRepositoryJDBCService implements DashboardMonitorRepositoryService {
	private  DashboardMonitorRepository  dashboardMonitorRepository;

	public DashboardMonitorRepository getDashboardMonitorRepository() {
		return this.dashboardMonitorRepository;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateEnableByRegion(String region, int enable) {
		Assert.isTrue(enable >= 0 && enable <=1, "The enable value has to be 0 (zero) or 1 (one)");
		return getDashboardMonitorRepository().updateEnableByRegion(region, enable);
	}

	@Override
	public void setDashboardMonitorRepository(DashboardMonitorRepository dashboardMonitorRepository) {
		this.dashboardMonitorRepository = dashboardMonitorRepository;
	}
}