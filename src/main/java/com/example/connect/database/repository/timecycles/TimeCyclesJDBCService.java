package com.example.connect.database.repository.timecycles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

public class TimeCyclesJDBCService implements TimeCyclesRepositoryService {

	private static final String NA = "NA";

	private TimeCyclesRepository timeCyclesRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> getWindowsAvailable(final String country, final String businessUnit) {
		String bu = businessUnit;
		if (bu == null) {
			bu = "";
		}

		final List<Map<String, Object>> listForContacts = this.timeCyclesRepository.getWindows();
		final List<Map<String, Object>> list = getWindows(country, bu, listForContacts);
		if (list == null) {
			return getWindows(country, NA, listForContacts);
		} else {
			return list;
		}
	}


	public List<Map<String, Object>> getWindows(final String country, final String businessUnit,
			final List<Map<String, Object>> list) {

		final List<Map<String, Object>> mapResultFound = new ArrayList<Map<String, Object>>();

		String countryAux;
		String bisnessUnitAux;
		for (final Map<String, Object> map : list) {
			countryAux = map.get("COUNTRY").toString();
			bisnessUnitAux = map.get("BUSINESS_UNIT").toString();

			if (country.equalsIgnoreCase(countryAux)) {
				if (businessUnit.equalsIgnoreCase(bisnessUnitAux)) {
					mapResultFound.add(map);
					return mapResultFound;
				}
			}
		}
		return null;
	}

	public void setTimeCyclesRepository(final TimeCyclesRepository timeCyclesRepository) {
		this.timeCyclesRepository = timeCyclesRepository;
	}

}
