package com.example.connect.database.repository.isales;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.isales.Allocation;

public class AllocationRepositoryJDBC extends JdbcDaoSupport implements AllocationRepository {

	public AllocationRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	private static class AllocationRowMapper implements RowMapper<Allocation> {
		@Override
		public Allocation mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final Allocation allocation = new Allocation();
			allocation.setAllocation(rs.getString("alocacion"));
			allocation.setAction(rs.getString("action"));
			return allocation;
		}
	}
		
	@Override
	public List<Allocation> findByAction(String action) {
		return getJdbcTemplate().query("select alocacion, action from auto_ALL_isales_proc_vw where action = ?", 
				new String[] { action }, 
				new AllocationRowMapper());
	}
}