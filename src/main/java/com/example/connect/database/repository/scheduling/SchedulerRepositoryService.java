package com.example.connect.database.repository.scheduling;

import java.sql.Timestamp;
import java.util.List;

import com.example.connect.database.model.scheduling.TaskProcess;


public interface SchedulerRepositoryService {
	List<TaskProcess> findTasksToRestartByRegion(String region);
	List<TaskProcess> findActiveTasksByName(String taskName, String region);
	TaskProcess findByTaskNameAndProcessId(String taskName, String region, String applicationName);
	int updateStartProcess(String taskName, String region);
	int disableRestartProcesses(List<TaskProcess> taskProcesses);
	int updateStartTime(String taskName, String region, String processId, Timestamp start);
	int updateCompletedProcess(
			String taskName, String region, String applicationName, Timestamp finish, String duration);
	int updateErrorProcess(String taskName, String region, String applicationName, Timestamp finish, 
			String duration, String errorMessage);
	int updateErrorDependenceProcess(String taskName, String region, String applicationName, Timestamp finish, 
			String duration, String errorMessage);
 	int updateRowsProcessed(String taskName, String region, String applicationName, int rowsProcessed, int rowsDeleted);
	void setSchedulerRepository(SchedulerRepository schedulerRepository);
	boolean areProcessesDependent(List<TaskProcess> processes);
	boolean existProcessesDependent(List<TaskProcess> processes);
	boolean existProcessesReRunning(List<TaskProcess> processes);
	int areAllProcessesFinished(String taskName, String region);
	int updateAllTaskStatusToCompleted();
	boolean checkDepencenciesError(String taskName, String region,String procId);
	int updateErrorPostLoadProcess(String taskName, String region, String errorMessage) ;

	
 }