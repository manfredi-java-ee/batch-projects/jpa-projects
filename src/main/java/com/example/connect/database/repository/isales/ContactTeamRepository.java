package com.example.connect.database.repository.isales;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.isales.ContactTeam;

@NoRepositoryBean
public interface ContactTeamRepository {

	/**
	 * Diferencias de Altas o Bajas en Isale de contact team.
	 * 
	 * @return list of added/removed contact team
	 */
	List<ContactTeam> findByAction(String action);

}
