package com.example.connect.database.repository.timecycles;

 
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class TimeCyclesRepositoryForAccountForDeleteJDBC extends JdbcDaoSupport implements TimeCyclesRepository {

	private static final String QUERY_WINDOWS_FOR_DELETE_ACCOUNT = " SELECT COUNTRY,NVL(BUSINESS_UNIT, 'NA') BUSINESS_UNIT,CYCLEYEAR,CYCLEID,CYCLESTART,CYCLEEND,DEL_STA_DT,DEL_END_DT,DEL_APP_DT, DEL_STA_DT_ACC,DEL_END_DT_ACC,DEL_APP_DT_ACC "
			+ " FROM TIMECYCLES "
			+ " WHERE trunc(sysdate) >= DEL_STA_DT_ACC AND trunc(sysdate) <= DEL_END_DT_ACC ";

    public TimeCyclesRepositoryForAccountForDeleteJDBC(final DataSource dataSource) {
		setDataSource(dataSource);
	}
 
	@Override
	public List<Map<String, Object>> getWindows() {
		return getJdbcTemplate().queryForList(QUERY_WINDOWS_FOR_DELETE_ACCOUNT);
	}

}
