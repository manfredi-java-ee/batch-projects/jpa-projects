package com.example.connect.database.repository.starbyproduct;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class StarByProductRepositoryJDBCForLA extends JdbcDaoSupport implements StarByProductRepository {

	public StarByProductRepositoryJDBCForLA(DataSource dataSource) {
		setDataSource(dataSource);
	}


	@Override
	public boolean isAudit(final String userId, final String contactId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT 1 ");
	    sql.append("FROM CONTACTS_STARPRODUCTS SBP ");
	    sql.append("INNER JOIN EMPLOYEES EMP ON (EMP.USERID=SBP.USERID AND EMP.USERSTATUS='Active') ");
	    sql.append("INNER JOIN PRODUCTS_R18 PRO ON (PRO.STATUS='Active' AND SBP.PRODUCTID=PRO.PRODUCTNAME) ");
	    sql.append("INNER JOIN PRODUCTS_ALLOCATION_R18 PA ON (PA.ALLOC_GROUP='Sales Team Allocation' AND PA.SALES_TEAM = EMP.SALESTEAMID AND PRO.PRODUCT_ID = PA.PRODUCTID AND PA.WEIGHT NOT IN('0','0.00')) ");
	    sql.append("WHERE SBP.USERID = ? ");
	    sql.append("AND SBP.CONTACTID = ? ");
	    sql.append("AND SBP.SBP_TYPE = 'SBP Per User' ");
	    sql.append("AND (SBP.SEGMENTID IN('A_Protect','B_Engage') or SBP.RXQUINTIL IN('1','2','3')) ");
	    
	    return !getJdbcTemplate().queryForList(
	    		sql.toString(), userId, contactId).isEmpty();
	}

}
