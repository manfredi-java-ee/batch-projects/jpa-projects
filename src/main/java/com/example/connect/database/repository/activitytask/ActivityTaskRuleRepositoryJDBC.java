package com.example.connect.database.repository.activitytask;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.activitytask.ActivityTaskRule;

public class ActivityTaskRuleRepositoryJDBC extends JdbcDaoSupport implements ActivityTaskRuleRepository {
	
	private String query = "SELECT * FROM TASK_BUSINESS_RULE WHERE STATUS = 'Active'";

	public ActivityTaskRuleRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

	private static class ActivityRowMapper implements RowMapper<ActivityTaskRule> {

		public ActivityTaskRule mapRow(final ResultSet rs, final int rowNum) throws SQLException {

			final ActivityTaskRule activityTaskRule = new ActivityTaskRule();
				activityTaskRule.setId(rs.getString(KeyRule.ID_RULE.getValue()));
				activityTaskRule.setCountry(rs.getString(KeyRule.COUNTRY.getValue()));
				activityTaskRule.setDescription(rs.getString(KeyRule.DESCRIPTION.getValue()));
				activityTaskRule.setRule(rs.getString(KeyRule.RULE.getValue()));
				activityTaskRule.setField(rs.getString(KeyRule.FIELD.getValue()));
				activityTaskRule.setBusinessUnit(rs.getString(KeyRule.BUSINESS_UNIT.getValue()));
				activityTaskRule.setParameter(rs.getString(KeyRule.PARAM.getValue()));
				activityTaskRule.setStatus(rs.getString(KeyRule.STATUS.getValue()));
				return activityTaskRule;
		}
	}

	public List<ActivityTaskRule> getBusinessRule() {
		return getJdbcTemplate().query(query, new ActivityRowMapper());
	}
	
	public List<Map<String, Object>> executeQuery(final String sql, final Map<String,String> param) {
		return getJdbcTemplate().queryForList(sql,new MapSqlParameterSource().addValues(param));
	}
	
		
	enum KeyRule {
		REGION("REGION"), 
		COUNTRY("COUNTRY"), 
		ID_RULE("ID_RULE"), 
		BUSINESS_UNIT("BUSINESS_UNIT"), 
		STATUS("STATUS"),
		DESCRIPTION("DESCRIPTION"), 
		RULE("RULE"), 
		FIELD("FIELD"), 
		PARAM("PARAMETER");

		private String value;
		private KeyRule(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	

}