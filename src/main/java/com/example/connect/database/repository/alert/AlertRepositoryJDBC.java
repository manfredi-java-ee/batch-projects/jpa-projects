package com.example.connect.database.repository.alert;

import java.sql.CallableStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class AlertRepositoryJDBC extends JdbcDaoSupport implements AlertRepository {
 
	
	public AlertRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

 
	@Override
	public boolean runAlert(String schema, String alertId, String alertGrpId,boolean logExecutionDate ) {
		
		String proc = "ALERT_REPORT.ALERT('" +schema +"','" +alertId + "','" +  alertGrpId + "', " + logExecutionDate + " )"; 
		
		try {
			CallableStatement cstmt = getJdbcTemplate().getDataSource().getConnection().prepareCall("{CALL " + proc + "}");
			cstmt.executeUpdate();
			return true;
		} catch (SQLException e1) {
			//logger.error(e1.fillInStackTrace().toString(),e1);
			return false;
		}
 
	}
	
 
}