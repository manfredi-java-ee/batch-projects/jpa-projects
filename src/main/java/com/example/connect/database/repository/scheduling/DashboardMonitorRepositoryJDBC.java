package com.example.connect.database.repository.scheduling;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class DashboardMonitorRepositoryJDBC extends JdbcDaoSupport implements DashboardMonitorRepository {
	private static final String UPDATE_DASHBOARD_MONITOR = "UPDATE dashb_monitor SET STATUS = 'N', status_cognos = 'N', enable = ? ";
	private static final String REGION_LA = "LA";
	
	public DashboardMonitorRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int updateEnableByRegion(String region, int enable) {
		StringBuilder sb = new StringBuilder(UPDATE_DASHBOARD_MONITOR);
		if (REGION_LA.equalsIgnoreCase(region)) {
			sb.append(" WHERE country != 'MX' and country != 'BR' and country != 'Puerto Rico2' ");
			return getJdbcTemplate().update(sb.toString(), 
					new Object[] { enable });
		} else {
			sb.append(" WHERE COUNTRY = ? ");
			return getJdbcTemplate().update(sb.toString(), 
					new Object[] { enable, region });			
		}
	}
}