package com.example.connect.database.repository.scheduling;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.scheduling.TaskActive;
import com.example.connect.database.model.scheduling.TaskProcess;
import com.example.connect.database.model.scheduling.TaskStatus;

@NoRepositoryBean
public interface SchedulerRepository {
	List<TaskProcess> findTasksToRestartByRegion(String region, TaskActive active);
	List<TaskProcess> findByTaskName(String taskName, String region, TaskActive active);
	TaskProcess findByTaskNameAndProcessId(String taskName, String region, String processId);
	int updateActiveTasksToInProcess(String taskName, String region);
	int disableRestartProcess(TaskProcess taskProcess);
	int updateStartTime(String taskName, String region, String processId, Timestamp start);
	int updateEndProcess(String taskName, String region, String processId, TaskStatus status, 
			Timestamp finish, String duration, String errorMessage);
	int updateRowsProcessed(String taskName, String region, String applicationName, int rowsProcessed,
			int rowsDeleted);
	List<TaskProcess> areAllProcessesFinished(final String taskName, final String region);
 	int updateAllTaskStatus(TaskStatus TaskStatus) ;
 	int checkDepencenciesError(final String taskName,final String procId,final String region);
	int updatePostLoadProcessError(final String taskName, final String region,final String errorMessage);
	


}
