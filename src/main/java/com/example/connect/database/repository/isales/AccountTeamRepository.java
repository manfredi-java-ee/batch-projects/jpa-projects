package com.example.connect.database.repository.isales;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.isales.AccountTeam;
 
@NoRepositoryBean
public interface AccountTeamRepository {

	/**
	 * Diferencias de Altas o Bajas en Isale de contact team.
	 * 
	 * @return list of added/removed contact team
	 */
	List<AccountTeam> findByAction(String action);

}
