package com.example.connect.database.repository.contact;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ContactRepositoryJDBCForMX extends JdbcDaoSupport implements ContactRepository {

	private String contactQuery;
	
	public ContactRepositoryJDBCForMX(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	public List<Map<String, Object>> findByCustomFields(String firstName, String lastName, String medicalId,
			String state, String userId) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM CONTACTS ");
	    sql.append("WHERE EXTID IS NOT NULL ");
	    sql.append("AND EXTID LIKE 'MX-%' ");
		sql.append("AND translate(upper(contactfirstname),'ÁÉÍÓÚÑ','AEIOUN') = translate(upper(?),'ÁÉÍÓÚÑ','AEIOUN') ");
   		sql.append("AND translate(upper(contactlastname),'ÁÉÍÓÚÑ','AEIOUN') = translate(upper(?),'ÁÉÍÓÚÑ','AEIOUN') ");

   		return getJdbcTemplate().queryForList(sql.toString(), firstName, lastName);
	}

	@Override
	public String getContactQuery() {
		return contactQuery;
	}

	public void setContactQuery(String contactQuery) {
		this.contactQuery = contactQuery;
	}

}
