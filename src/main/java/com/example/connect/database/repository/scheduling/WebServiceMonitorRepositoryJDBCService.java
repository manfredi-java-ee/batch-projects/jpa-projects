package com.example.connect.database.repository.scheduling;

import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.scheduling.WebServiceMonitor;


public class WebServiceMonitorRepositoryJDBCService implements WebServiceMonitorRepositoryService {
	private  WebServiceMonitorRepository  webServiceMonitorRepository;

	@Transactional(readOnly = true)
	public WebServiceMonitor findByWsName(String wsName) {
		return getWebServiceMonitorRepository().findByWsName(wsName);
	}
	
	public WebServiceMonitorRepository getWebServiceMonitorRepository() {
		return webServiceMonitorRepository;
	}

	public void setWebServiceMonitorRepository(WebServiceMonitorRepository webServiceMonitorRepository) {
		this.webServiceMonitorRepository = webServiceMonitorRepository;
	}
}