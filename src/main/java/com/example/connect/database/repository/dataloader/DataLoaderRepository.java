package com.example.connect.database.repository.dataloader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.dataloader.ParameterLoaderDB;

@NoRepositoryBean
public interface DataLoaderRepository {

	public ResultSet findExtraction(final String sql, final String region, final String action) throws SQLException;

	public List<ParameterLoaderDB> findParameter() throws SQLException;
	
	public int infoRowProcess(final String id, final int row, final String link, final String status) throws SQLException;
	
}