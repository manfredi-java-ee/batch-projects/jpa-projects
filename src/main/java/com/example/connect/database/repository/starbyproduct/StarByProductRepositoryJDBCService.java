package com.example.connect.database.repository.starbyproduct;

import org.springframework.transaction.annotation.Transactional;

public class StarByProductRepositoryJDBCService implements StarByProductRepositoryService {

	private StarByProductRepository starByProductRepository;

	@Override
	@Transactional(readOnly = true)
	public boolean isAudit(final String userId, final String contactId) {
		return starByProductRepository.isAudit(userId, contactId);
	}

	public void setStarByProductRepository(final StarByProductRepository starByProductRepository) {
		this.starByProductRepository = starByProductRepository;
	}
	
}
