package com.example.connect.database.repository.employess;

import java.util.List;
import java.util.Map;

import org.springframework.data.repository.NoRepositoryBean;

 
@NoRepositoryBean
public interface EmployessRepository {

	
	List<Map<String, Object>> findByRowId(final String rowId);
	
	List<Map<String, Object>> findByExternalId(final String externalId);
}
