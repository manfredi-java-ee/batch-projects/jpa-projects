package com.example.connect.database.repository.multilanguaje;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.languaje.Multilanguaje;
import com.example.connect.database.model.languaje.MultilanguajeType;

 
@NoRepositoryBean
public interface MultilanguajeRepository {

	
	List<Multilanguaje> getTranslationForType(MultilanguajeType type);
	
 }
