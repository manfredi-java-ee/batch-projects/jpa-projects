package com.example.connect.database.repository.dataloader;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.dataloader.ParameterLoaderDB;

public class DataLoaderRepositoryJDBC extends JdbcDaoSupport implements	DataLoaderRepository {

	private final static String LA = "'Ecuador', 'Argentina', 'Cenam', 'Peru', 'Chile'";
	private final static String MX = "'Mexico'";
	private final static String BR = "'Brasil'";
	private final static String REPLAY_ACTION = "$$Action$$";
	private final static String REPLAY_REGION = "$$Region$$";
	private static String sqlUpdateRowNomber = "UPDATE CTRL_CRM_LOADER set ROW_NUM=?, LINK_CRM=?, STATUS=?, LAST_RUN_DATE=SYSDATE WHERE ID=?";
	private String sqlGetParameter = "SELECT CT.*, RE.QRY AS SQL FROM CTRL_CRM_LOADER CT"
								   +" INNER JOIN SODM_REPORT RE ON RE.ID=CT.ID_REPORT WHERE CT.STATUS IN ('ACTIVE', 'ERROR')";
	

	@Override
	public ResultSet findExtraction(final String sql, final String region, final String action) throws SQLException {
		String sqlWK = sql.replace(REPLAY_REGION, getRegion(region)).replace(REPLAY_ACTION, "'"+action.toUpperCase()+"'");  
		PreparedStatement st = getJdbcTemplate().getDataSource().getConnection().prepareStatement(sqlWK);
		st.setFetchSize(1000);
		return st.executeQuery();
	}

	@Override
	public List<ParameterLoaderDB> findParameter() throws SQLException {
		return getJdbcTemplate().query(sqlGetParameter, new ParameterRowMapper());
	}
	
	@Override
	
	public int infoRowProcess(final String id, final int row, final String link, final String status) throws SQLException {
		return getJdbcTemplate().update(sqlUpdateRowNomber, new Object[] {row, link, status, id});
		
	}

	public String getSqlParameter() {
		return sqlGetParameter;
	}

	public void setSqlParameter(String sqlParameter) {
		this.sqlGetParameter = sqlParameter;
	}

	private String getRegion(String region){
		if(region.equalsIgnoreCase("MX")){
			return MX;
		}else if(region.equalsIgnoreCase("BR")){
			return BR;
		}else{
			return LA;
		}
	}
	
	private static class ParameterRowMapper implements RowMapper<ParameterLoaderDB> {
		@Override
		public ParameterLoaderDB mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			
			ParameterLoaderDB parameter = new ParameterLoaderDB();
			parameter.setClientloglevel(rs.getString("CLIENTLOGLEVEL"));
			parameter.setCsvdelimiter(rs.getString("CSVDELIMITER"));
			parameter.setDatafilepath(rs.getString("DATAFILEPATH"));
			parameter.setDatetimeformat(rs.getString("DATETIMEFORMAT"));
			parameter.setDuplicatecheckoption(rs.getString("DUPLICATECHECKOPTION"));
			parameter.setImportloglevel(rs.getString("IMPORTLOGLEVEL"));
			parameter.setClientlogfiledir(rs.getString("CLIENTLOGFILEDIR"));
			parameter.setHosturl(rs.getString("HOSTURL"));
			parameter.setMapfilepath(rs.getString("MAPFILEPATH"));
			parameter.setOperation(rs.getString("OPERATION"));
			parameter.setRecordtype(rs.getString("RECORDTYPE"));
			parameter.setRegion(rs.getString("REGION"));
			parameter.setEntity(rs.getString("ENTITY_NAME"));
			parameter.setResumerequest(null);
			parameter.setUserName(rs.getString("USERNAME"));
			parameter.setUserPwd(rs.getString("USERPWD"));
			parameter.setSql(rs.getString("SQL"));
			parameter.setWaitforcompletion(rs.getString("WAITFORCOMPLETION"));
			parameter.setId(rs.getString("ID"));
			parameter.setRowCount(rs.getInt("ROW_NUM"));
			return parameter;
		}
	}
}