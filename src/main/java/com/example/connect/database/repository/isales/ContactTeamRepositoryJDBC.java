package com.example.connect.database.repository.isales;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.example.connect.database.model.isales.ContactTeam;

public class ContactTeamRepositoryJDBC extends JdbcDaoSupport implements ContactTeamRepository {

	public ContactTeamRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	private static class ContactTeamRowMapper implements RowMapper<ContactTeam> {
		@Override
		public ContactTeam mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final ContactTeam contactTeam = new ContactTeam();
			contactTeam.setId(rs.getString("ID"));
			contactTeam.setUserId(rs.getString("userid"));
			contactTeam.setContactid(rs.getString("contactid"));
			contactTeam.setAction(rs.getString("action"));

			return contactTeam;
		}
	}
		
	@Override
	public List<ContactTeam> findByAction(String action) {
		return getJdbcTemplate().query("select ID, userid, contactid, action from auto_ct_isales_proc_vw where action = ?", 
				new String[] { action }, 
				new ContactTeamRowMapper());
	}
}