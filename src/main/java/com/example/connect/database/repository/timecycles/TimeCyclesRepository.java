package com.example.connect.database.repository.timecycles;

import java.util.List;
import java.util.Map;

import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface TimeCyclesRepository {

	List<Map<String, Object>> getWindows();

}
