package com.example.connect.database.repository.multilanguaje;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.example.connect.database.model.languaje.Multilanguaje;
import com.example.connect.database.model.languaje.MultilanguajeType;



public class MultilanguajeJDBCService implements MultilanguajeRepositoryService, InitializingBean {
	
 	
	private MultilanguajeRepository multilanguajeRepository;
	private Map<String, Map<String, Multilanguaje>> multilanguajeMessagesMap = null;
	private Map<String, Map<String, Multilanguaje>>  multilanguajeFieldsMap = null;
	
	
	 
	
	@Transactional(readOnly = true)
	public Multilanguaje getTraslationMessagesById(final String id, final String country) {
	 	final Map<String, Multilanguaje> map = multilanguajeMessagesMap.get(country);
		if (map != null) {
			return multilanguajeMessagesMap.get(country).get(id);
		} else {
			return null;
		}
	}
	
	
	
	@Transactional(readOnly = true)
	public String getTraslationFields(final List<String> fieldsToTraslate ,final String country) {
		
	 	
	 	Map<String, Multilanguaje> map = multilanguajeFieldsMap.get(country);
		
		
		if (map != null) {
			
			List<String> filedTraslated  = Lists.newArrayList(); ;
			
			  for (String field : fieldsToTraslate) {
				 
				 Multilanguaje multi = multilanguajeFieldsMap.get(country).get(field);
				 
				Assert.notNull(multi, "The traslation for field " + field
						+ " not found for country : " + country );
				 
				filedTraslated.add(multi.getTraslation());
		 	}
			 				
			  return filedTraslated.toString();
			
		} else {
			return null;
		}
	 	 	
	}
	
	
	

	@Override
	public void afterPropertiesSet() throws Exception {
 
		
		if (multilanguajeMessagesMap == null) {
			
			
			List<Multilanguaje> multilanguajeMessagesList = multilanguajeRepository
					.getTranslationForType(MultilanguajeType.MESSAGE);

			multilanguajeMessagesMap = new HashMap<String, Map<String, Multilanguaje>>();

			for (Multilanguaje multi : multilanguajeMessagesList) {

				if (multilanguajeMessagesMap.containsKey(multi.getCountry())) {
					Map<String, Multilanguaje> labels = multilanguajeMessagesMap.get(multi
							.getCountry());
					labels.put(multi.getId(), multi);
				} else {
					Map<String, Multilanguaje> labels = new HashMap<String, Multilanguaje>();
					labels.put(multi.getId(), multi);
					multilanguajeMessagesMap.put(multi.getCountry(), labels);
				}
			}

		}
		
		
		MapUtils.debugPrint(System.out, "multilanguajeMessagesMap", multilanguajeMessagesMap);
		
		if (multilanguajeFieldsMap == null) {
				
				List<Multilanguaje> multilanguajeFieldList = multilanguajeRepository
						.getTranslationForType(MultilanguajeType.FIELD);
	
				multilanguajeFieldsMap = new HashMap<String, Map<String, Multilanguaje>>();
	
				for (Multilanguaje multi : multilanguajeFieldList) {
	
					if (multilanguajeFieldsMap.containsKey(multi.getCountry())) {
						Map<String, Multilanguaje> labels = multilanguajeFieldsMap.get(multi
								.getCountry());
						labels.put(multi.getId(), multi);
					} else {
						Map<String, Multilanguaje> labels = new HashMap<String, Multilanguaje>();
						labels.put(multi.getId(), multi);
						multilanguajeFieldsMap.put(multi.getCountry(), labels);
					}
				}
	
			}
	
		MapUtils.debugPrint(System.out, "multilanguajeFieldsMap", multilanguajeFieldsMap);
		
	}
	
	
	
	
	public void setMultilanguajeRepository(MultilanguajeRepository multilanguajeRepository) {
		this.multilanguajeRepository = multilanguajeRepository;
	}

	
 
}