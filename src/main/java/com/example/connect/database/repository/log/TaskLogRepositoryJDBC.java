package com.example.connect.database.repository.log;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class TaskLogRepositoryJDBC extends JdbcDaoSupport implements TaskLogRepository {

	public TaskLogRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
		
	@Override
	public int log(String name, String objectId,String logType,String logMessage,String adminStatus, String mgrAprovStatus,String status) {
		return getJdbcTemplate().update(
				"INSERT INTO TASK_LOG_TBL (NAME, OBJECT_ID, LOG_TYPE, LOG_MESSAGE,ADMIN_STATUS,MGR_APROV_STATUS,STATUS,LOG_DATE)" 
				+  "VALUES (?, ?, ?, ?, ?,?, ?,sysdate)", 
				
				new Object[] { name ,objectId, logType, logMessage, adminStatus,mgrAprovStatus,status  });
	}
}