package com.example.connect.database.repository.scheduling;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DashboardMonitorRepository {
	int updateEnableByRegion(String region, int enable);

}
