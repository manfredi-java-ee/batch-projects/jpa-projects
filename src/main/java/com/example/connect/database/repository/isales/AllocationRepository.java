package com.example.connect.database.repository.isales;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.example.connect.database.model.isales.Allocation;

@NoRepositoryBean
public interface AllocationRepository {

	/**
	 * Diferencias de Altas o Bajas en Isale de Allocation.
	 * 
	 * @return list of added/removed contact team
	 */
	List<Allocation> findByAction(String action);

}
