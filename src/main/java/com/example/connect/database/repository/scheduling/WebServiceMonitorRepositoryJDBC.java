package com.example.connect.database.repository.scheduling;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.scheduling.WebServiceMonitor;

public class WebServiceMonitorRepositoryJDBC extends JdbcDaoSupport implements WebServiceMonitorRepository {
	private static final String BASE_QUERY_APP_TASK_SCHEDULER = "SELECT WS_NAME, DEL_ITEM_NAME, EX_DATE, SYNC_DATE, "
			+ "EX_DURATION, ROWS_PROCESSED, ROWS_DELETED, EX_STATUS, CRM_STATUS, WIMP_STATUS " + "FROM WS_MONITOR";

	private static final String FAILED_STATUS = "FAILED";

	public WebServiceMonitorRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	@Transactional(readOnly = true)
	public WebServiceMonitor findByWsName(String wsName) {
		StringBuilder sb = new StringBuilder(BASE_QUERY_APP_TASK_SCHEDULER)
				.append(" where WS_NAME = ? AND CRM_STATUS != ? AND WIMP_STATUS != ? ");

		final List<WebServiceMonitor> list = getJdbcTemplate().query(sb.toString(),
				new String[] { wsName, FAILED_STATUS, FAILED_STATUS }, new WebServiceMonitorRowMapper());
		return list.isEmpty() ? null : list.get(0);
	}

	private static class WebServiceMonitorRowMapper implements RowMapper<WebServiceMonitor> {
		@Override
		public WebServiceMonitor mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final WebServiceMonitor webServiceMonitor = new WebServiceMonitor();
			webServiceMonitor.setWsName(rs.getString("WS_NAME"));
			webServiceMonitor.setDelItemName(rs.getString("DEL_ITEM_NAME"));
			webServiceMonitor.setExDate(rs.getDate("EX_DATE"));
			webServiceMonitor.setSyncDate(rs.getDate("SYNC_DATE"));
			webServiceMonitor.setDuration(rs.getString("EX_DURATION"));
			webServiceMonitor.setRowsProcessed(rs.getInt("ROWS_PROCESSED"));
			webServiceMonitor.setRowsDeleted(rs.getInt("ROWS_DELETED"));
			webServiceMonitor.setExStatus(rs.getString("EX_STATUS"));
			webServiceMonitor.setCrmStatus(rs.getString("CRM_STATUS"));
			webServiceMonitor.setWimpStatus(rs.getString("WIMP_STATUS"));

			return webServiceMonitor;
		}
	}
}