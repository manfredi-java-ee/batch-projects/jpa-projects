package com.example.connect.database.repository.activitytask;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.connect.database.model.activitytask.ActivityTaskRule;
import com.google.common.collect.ImmutableMap;
import com.example.connect.database.repository.account.AccountRepository;
import com.example.connect.database.repository.contact.ContactRepository;
import com.example.connect.database.repository.employess.EmployessRepository;
import com.example.connect.database.repository.log.TaskLogRepository;

public class TaskRepositoryService {

	private ActivityTaskRuleRepository activityTaskRuleRepository;
	private EmployessRepository employessRepository;
	private ContactRepository contactRepository;
	private AccountRepository accountRepository;

	private TaskLogRepository taskLogRepository;
	private List<ActivityTaskRule> ruleList = null;
	private static final String NA = "NA";

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int log(final String name, final String objectId, final String logType, final String logMessage,
			final String adminStatus, final String mgrAprovStatus, final String status) {
		return taskLogRepository.log(name, objectId, logType, logMessage, adminStatus, mgrAprovStatus, status);
	}

	public ActivityTaskRule getRule(final String idRule, final String country, final String bu) {

		ActivityTaskRule rule = searhRule(idRule, country, bu);
		if (rule == null) {
			rule = searhRule(idRule, country, NA);
		}
		if (rule == null) {
			rule = searhRule(idRule, NA, NA);
		}
		return rule;
	}

	private ActivityTaskRule searhRule(final String id, final String country, final String bu) {
		getAllRule();
		String countryRule;
		String buRule;
		String idRule;

		for (ActivityTaskRule rule : ruleList) {
			countryRule = rule.getCountry();
			buRule = rule.getBusinessUnit();
			idRule = rule.getId();

			if (idRule.equalsIgnoreCase(id)) {
				if (countryRule.equalsIgnoreCase(country)) {
					if (buRule.equalsIgnoreCase(bu)) {
						return rule;
					}
				}
			}
		}
		return null;
	}

	public List<Map<String, Object>> executeQuery(final String sql, final Map<String, String> param) {
		return activityTaskRuleRepository.executeQuery(sql, param);
	}

	public Map<String, Object> findEmployessByRowId(final String rowId) {

		List<Map<String, Object>> employees = employessRepository.findByRowId(rowId);

		if (employees.size() > 0) {
			return employees.get(0);
		} else {
			return null;
		}

	}

	public Map<String, Object> findEmployessByExternalId(final String externalId) {
		final List<Map<String, Object>> employees = employessRepository.findByExternalId(externalId);
		if (employees.size() > 0) {
			return employees.get(0);
		} else {
			return null;
		}

	}

	public String getContactQuery() {
		return contactRepository.getContactQuery();
	}

	public Map<String, Object> findContactByCustomFields(final String firstName, 
			final String lastName, final String medicalId, final String state, final String userId) {
		final List<Map<String, Object>> contacts = contactRepository.findByCustomFields(
				firstName, lastName, medicalId, state, userId);
		if (contacts.isEmpty()) {
			return ImmutableMap.of();
		} else {
			return contacts.get(0);
		}
	}

	public String getAccountQuery() {
		return accountRepository.getAccountQuery();
	}

	public Map<String, Object> findAccountByCustomFields(
			final String accountName, final String colony, final String cnpj) {
		final List<Map<String, Object>> accounts = accountRepository.findByCustomFields(accountName, colony, cnpj);
		if (accounts.isEmpty()) {
			return ImmutableMap.of();
		} else {
			return accounts.get(0);
		}
	}


	private void getAllRule() {
		if (ruleList == null) {
			ruleList = activityTaskRuleRepository.getBusinessRule();
		}
	}

	public ActivityTaskRuleRepository getActivityTaskRuleRepository() {
		return activityTaskRuleRepository;
	}

	public void setActivityTaskRuleRepository(ActivityTaskRuleRepository activityTaskRuleRepository) {
		this.activityTaskRuleRepository = activityTaskRuleRepository;
	}

	public void setTaskLogRepository(TaskLogRepository taskLogRepository) {
		this.taskLogRepository = taskLogRepository;
	}

	public void setEmployessRepository(EmployessRepository employessRepository) {
		this.employessRepository = employessRepository;
	}

	public void setContactRepository(ContactRepository newContactRepository) {
		this.contactRepository = newContactRepository;
	}

	public void setAccountRepository(AccountRepository newAccountRepository) {
		this.accountRepository = newAccountRepository;
	}

}