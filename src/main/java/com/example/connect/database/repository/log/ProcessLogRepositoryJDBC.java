package com.example.connect.database.repository.log;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ProcessLogRepositoryJDBC extends JdbcDaoSupport implements ProcessLogRepository {

	public ProcessLogRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
		
	@Override
	public int log(String processId, String type, String description, String message) {
		return getJdbcTemplate().update(
				"INSERT INTO PROC_LOG_TBL (PROC_ID, LOG_TYP, LOG_CD, LOG_MSG, LOG_DT) VALUES (?, ?, ?, ?, sysdate)", 
				new Object[] { processId, type, description, message });
	}
}