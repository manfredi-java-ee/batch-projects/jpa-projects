package com.example.connect.database.repository.employess;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class EmployessRepositoryJDBC extends JdbcDaoSupport implements EmployessRepository {
	
 
	public EmployessRepositoryJDBC(DataSource dataSource) {
		setDataSource(dataSource);
	}
 
	public List<Map<String, Object>> findByRowId(final String rowId) {
		
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM EMPLOYEES ");
	    sql.append("WHERE USERID= ? ");
		 
		return getJdbcTemplate().queryForList(sql.toString(), rowId);

	}
	
	 
	public List<Map<String, Object>> findByExternalId(final String externalId) {
	     
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
	    sql.append("FROM EMPLOYEES ");
	    sql.append("WHERE EXTID = ? ");
  	
		return getJdbcTemplate().queryForList(sql.toString(),externalId);
	
	}
	
	
	
	
	
	

}