package com.example.connect.database.model.log;

import java.util.EnumSet;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public enum EnumLogType {
	
	INFO("INFO"),             
	ERROR("ERROR");	      
 
 	private static final BiMap<EnumLogType, String> nameToValueMap = HashBiMap.create();

    static {
        for (EnumLogType value : EnumSet.allOf(EnumLogType.class)) {
            nameToValueMap.put(value, value.getType());
        }
    }
    private final String type;

    private EnumLogType(final String newStatus) {
        this.type = newStatus;
    }
    
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return this.getType();
    }

    public static EnumLogType getEnum(String enumName) {
    	return nameToValueMap.inverse().get(enumName);
    }

}
