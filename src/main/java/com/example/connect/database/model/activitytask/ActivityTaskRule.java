package com.example.connect.database.model.activitytask;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ActivityTaskRule {
	private String id;
	private String country;
	private String region;
	private String businessUnit;
	private String status;
	private String parameter;

	private String description;
	private String rule;
	private String field;

	public String getId() {
		return id;
	}

	public String getCountry() {
		return country;
	}

	public String getRegion() {
		return region;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public String getDescription() {
		return description;
	}

	public String getRule() {
		return rule;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public boolean isActive() {
		return RuleStatus.Active.toString().equalsIgnoreCase(this.getStatus());
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public boolean applyRule() {
		return BooleanUtils.toBoolean(
				StringUtils.defaultIfEmpty(getField(), "false").toLowerCase(), 
				Boolean.TRUE.toString(), Boolean.FALSE.toString());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("country", country)
				.append("region", region)
				.append("businessUnit", businessUnit)
				.append("status", status)
				.append("parameter", parameter)
				.append("description", description)
				.append("rule", rule)
				.append("field", field).toString();
	}

}
