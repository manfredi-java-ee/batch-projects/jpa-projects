package com.example.connect.database.model.scheduling;

public enum ProcessType {
	WS,
	PROC, 
	JAVA;
}