package com.example.connect.database.model.scheduling;

public enum TaskStatus {

	RESTART,
	IN_PROGRESS,
	COMPLETED,
	ERROR,
	ERROR_DEPENDENCIES;

}
