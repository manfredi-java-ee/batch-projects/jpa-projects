package com.example.connect.database.model.scheduling;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class WebServiceMonitor {
	private String wsName;
	private String delItemName;
	private Date exDate;
	private Date syncDate;
	private String duration;
	private int rowsProcessed;
	private int rowsDeleted;
	private String exStatus;
	private String crmStatus;
	private String wimpStatus;

	public WebServiceMonitor() {
		super();
	}

	public WebServiceMonitor(String wsName, String delItemName, Date exDate, Date syncDate, String duration,
			int rowsProcessed, int rowsDeleted, String exStatus, String crmStatus, String wimpStatus) {
		super();
		this.wsName = wsName;
		this.delItemName = delItemName;
		this.exDate = exDate;
		this.syncDate = syncDate;
		this.duration = duration;
		this.rowsProcessed = rowsProcessed;
		this.rowsDeleted = rowsDeleted;
		this.exStatus = exStatus;
		this.crmStatus = crmStatus;
		this.wimpStatus = wimpStatus;
	}
	
	public String getWsName() {
		return wsName;
	}

	public void setWsName(String wsName) {
		this.wsName = wsName;
	}

	public String getDelItemName() {
		return delItemName;
	}

	public void setDelItemName(String delItemName) {
		this.delItemName = delItemName;
	}

	public Date getExDate() {
		return exDate;
	}

	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	public Date getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(Date syncDate) {
		this.syncDate = syncDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public int getRowsProcessed() {
		return rowsProcessed;
	}

	public void setRowsProcessed(int rowsProcessed) {
		this.rowsProcessed = rowsProcessed;
	}

	public int getRowsDeleted() {
		return rowsDeleted;
	}

	public void setRowsDeleted(int rowsDeleted) {
		this.rowsDeleted = rowsDeleted;
	}

	public String getExStatus() {
		return exStatus;
	}

	public void setExStatus(String exStatus) {
		this.exStatus = exStatus;
	}

	public String getCrmStatus() {
		return crmStatus;
	}

	public void setCrmStatus(String crmStatus) {
		this.crmStatus = crmStatus;
	}

	public String getWimpStatus() {
		return wimpStatus;
	}

	public void setWimpStatus(String wimpStatus) {
		this.wimpStatus = wimpStatus;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("wsName", wsName)
				.append("exStatus", exStatus)
				.append("crmStatus", crmStatus)
				.append("wimpStatus", wimpStatus)
				.append("rowsProcessed", rowsProcessed)
				.append("rowsDeleted", rowsDeleted).toString();
	}
}