package com.example.connect.database.model.languaje;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Multilanguaje {
	private String id;
	private String languaje;
	private String type;
	private String traslation;
	private String country;
 
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLanguaje() {
		return languaje;
	}


	public void setLanguaje(String languaje) {
		this.languaje = languaje;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTraslation() {
		return traslation;
	}


	public void setTraslation(String traslation) {
		this.traslation = traslation;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("languaje", languaje)
				.append("type", type)
				.append("traslation", traslation)
				.append("country", country)
				.toString();
	}
}