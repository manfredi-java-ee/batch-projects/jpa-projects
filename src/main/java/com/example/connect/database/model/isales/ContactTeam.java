package com.example.connect.database.model.isales;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ContactTeam {
	private String id;
	private String userId;
	private String contactid;
	private String action;
    
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getContactid() {
		return contactid;
	}
	
	public void setContactid(String contactid) {
		this.contactid = contactid;
	}
    
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("userId", userId)
				.append("contactid", contactid)
				.append("action", action).toString();
	}
}