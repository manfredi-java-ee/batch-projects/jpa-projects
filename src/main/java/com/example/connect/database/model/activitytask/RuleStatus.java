package com.example.connect.database.model.activitytask;

public enum RuleStatus {

	Active,
	Inactive;

}
