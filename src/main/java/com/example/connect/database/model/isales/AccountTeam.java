package com.example.connect.database.model.isales;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AccountTeam {
	private String id;
	private String userId;
	private String accounid;
	private String action;
    
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getAccountid() {
		return accounid;
	}
	
	public void setAccountid(String contactid) {
		this.accounid = contactid;
	}
    
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("userId", userId)
				.append("accounid", accounid)
				.append("action", action).toString();
	}
}