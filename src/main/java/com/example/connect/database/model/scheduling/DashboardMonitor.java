package com.example.connect.database.model.scheduling;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DashboardMonitor {
	private String status; // Y - N
	private int enable; //0 - 1
	private String statusCognos; // Y - N
	private String country; //BR, MX, PE, AR, Puerto Rico, etc
	
	public DashboardMonitor() {
		super();
	}
	
	public DashboardMonitor(String status, int enable, String statusCognos, String country) {
		super();
		this.status = status;
		this.enable = enable;
		this.statusCognos = statusCognos;
		this.country = country;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getEnable() {
		return enable;
	}

	public void setEnable(int enable) {
		this.enable = enable;
	}

	public String getStatusCognos() {
		return statusCognos;
	}

	public void setStatus_cognos(String statusCognos) {
		this.statusCognos = statusCognos;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}	

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("status", status)
				.append("enable", enable)
				.append("statusCognos", statusCognos)
				.append("country", country).toString();
	}
}