package com.example.connect.database.model.dataloader;

public class ParameterLoaderDB {

	private String id;
	private String region;
	private String operation;
	private String clientlogfiledir;
	private String clientloglevel;
	private String datafilepath;
	private String mapfilepath;
	private String recordtype;
	private String duplicatecheckoption;
	private String datetimeformat;
	private String csvdelimiter;
	private String importloglevel;
	private String waitforcompletion;
	private String resumerequest;
	private String requestid;
	private String entity;
	private String userName;
	private String userPwd;
	private String hosturl;
	private String sql;
	private int rowCount;

	final static String characterset = "UTF-8";
	final static String proxyserver = "webproxy.example.com";
	final static String proxyserverport = "8080";

	public String getOperation() {
		return operation;
	}

	public String getClientlogfiledir() {
		return clientlogfiledir;
	}

	public String getClientloglevel() {
		return clientloglevel;
	}

	public String getDatafilepath() {
		return datafilepath;
	}

	public String getMapfilepath() {
		return mapfilepath;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public String getDuplicatecheckoption() {
		return duplicatecheckoption;
	}

	public String getDatetimeformat() {
		return datetimeformat;
	}

	public String getCsvdelimiter() {
		return csvdelimiter;
	}

	public String getImportloglevel() {
		return importloglevel;
	}

	public String getWaitforcompletion() {
		return waitforcompletion;
	}

	public String getResumerequest() {
		return resumerequest;
	}

	public String getCharacterset() {
		return characterset;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public void setClientlogfiledir(String clientlogfiledir) {
		this.clientlogfiledir = clientlogfiledir;
	}

	public void setClientloglevel(String clientloglevel) {
		this.clientloglevel = clientloglevel;
	}

	public void setDatafilepath(String datafilepath) {
		this.datafilepath = datafilepath;
	}

	public void setMapfilepath(String mapfilepath) {
		this.mapfilepath = mapfilepath;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public void setDuplicatecheckoption(String duplicatecheckoption) {
		this.duplicatecheckoption = duplicatecheckoption;
	}

	public void setDatetimeformat(String datetimeformat) {
		this.datetimeformat = datetimeformat;
	}

	public void setCsvdelimiter(String csvdelimiter) {
		this.csvdelimiter = csvdelimiter;
	}

	public void setImportloglevel(String importloglevel) {
		this.importloglevel = importloglevel;
	}

	public void setWaitforcompletion(String waitforcompletion) {
		this.waitforcompletion = waitforcompletion;
	}

	public void setResumerequest(String resumerequest) {
		this.resumerequest = resumerequest;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRequestid() {
		return requestid;
	}

	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getHosturl() {
		return hosturl;
	}

	public void setHosturl(String hosturl) {
		this.hosturl = hosturl;
	}
	
	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
