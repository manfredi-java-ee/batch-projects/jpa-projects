package com.example.connect.database.model.log;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProcessLog {

	private String processId;
	private String type;
	private String description;
	private String message;
	private Date logDate;


	public String getProcessId() {
		return processId;
	}
	
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCd(String description) {
		this.description = description;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("processId", processId)
				.append("type", type)
				.append("description", description)
				.append("message", message)
				.append("logDate", logDate).toString();
	}
}