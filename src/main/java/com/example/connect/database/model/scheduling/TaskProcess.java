package com.example.connect.database.model.scheduling;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TaskProcess {
	// LA, BR, MX
	private String taskName;
	private String region;
	private ProcessType type;
	private String processId;
	private TaskStatus status;
	private String location;
	private String applicationName;
	private String arguments;
	private String rep;
	private int sequentialOrder;
	private int processExitStatus;
	private int rowsProcessed;
	private int rowsDeleted;
	private Timestamp start;
	private Timestamp end;
	private String duration;
	private TaskActive active = TaskActive.N; // Y or N
	private TaskReRunning rerunning = TaskReRunning.N;
	
	
	private String errorMessage;
	private String description;


	public TaskProcess() {
		super();
	}

	public TaskProcess(final String taskName, final String region, final ProcessType type,
			final String processId, final TaskStatus status, final String location,
			final String applicationName, final String arguments, final String rep,
			final int sequentialOrder, final int processExitStatus, int rowsProcessed, 
			final int rowsDeleted, final Timestamp start, final Timestamp end, final String duration,
			final TaskActive active, final String errorMessage, final String description,final TaskReRunning rerunning) {

		super();
		this.taskName = taskName;
		this.region = region;
		this.type = type;
		this.processId = processId;
		this.status = status;
		this.location = location;
		this.applicationName = applicationName;
		this.arguments = arguments;
		this.rep = rep;
		this.sequentialOrder = sequentialOrder;
		this.processExitStatus = processExitStatus;
		this.rowsProcessed = rowsProcessed;
		this.rowsDeleted = rowsDeleted;
		this.start = start;
		this.end = end;
		this.duration = duration;
		this.active = active;
		this.rerunning = rerunning;
		this.errorMessage = errorMessage;
		this.description = description;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(final String taskName) {
		this.taskName = taskName;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(final String region) {
		this.region = region;
	}

	public ProcessType getType() {
		return type;
	}

	public void setType(final ProcessType type) {
		this.type = type;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(final String processId) {
		this.processId = processId;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(final TaskStatus status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(final String location) {
		this.location = location;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(final String applicationName) {
		this.applicationName = applicationName;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(final String arguments) {
		this.arguments = arguments;
	}

	public String getRep() {
		return rep;
	}

	public void setRep(final String rep) {
		this.rep = rep;
	}

	public int getSequentialOrder() {
		return sequentialOrder;
	}

	public void setSequentialOrder(final int sequentialOrder) {
		this.sequentialOrder = sequentialOrder;
	}

	public int getProcessExitStatus() {
		return processExitStatus;
	}

	public void setProcessExitStatus(final int processExitStatus) {
		this.processExitStatus = processExitStatus;
	}

	public int getRowsProcessed() {
		return rowsProcessed;
	}

	public void setRowsProcessed(final int rowsProcessed) {
		this.rowsProcessed = rowsProcessed;
	}

	public int getRowsDeleted() {
		return rowsDeleted;
	}

	public void setRowsDeleted(final int rowsDeleted) {
		this.rowsDeleted = rowsDeleted;
	}

	public Timestamp getStart() {
		return start;
	}

	public void setStart(final Timestamp start) {
		this.start = start;
	}

	public Timestamp getEnd() {
		return end;
	}

	public void setEnd(final Timestamp end) {
		this.end = end;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(final String duration) {
		this.duration = duration;
	}

	public TaskActive getActive() {
		return active;
	}

	public void setActive(final TaskActive active) {
		this.active = active;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public TaskReRunning getRerunning() {
		return rerunning;
	}

	public void setRerunning(TaskReRunning rerunning) {
		this.rerunning = rerunning;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("region", region)
				.append("taskName", taskName)
				.append("location", location)
				.append("applicationName", applicationName)
				.append("arguments", arguments)
				.append("status", status)
				.append("rep", rep)
				.append("sequentialOrder", sequentialOrder)
				.append("processId", processId)
				.append("active", active)
				.append("rerunning", rerunning)
				.append("type", type)
				.append("description", description)
				.toString();
	}

}
