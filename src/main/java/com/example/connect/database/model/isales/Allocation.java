package com.example.connect.database.model.isales;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Allocation {
	private String allocation;
	private String action;

	public String getAllocation() {
		return allocation;
	}

	public void setAllocation(String allocation) {
		this.allocation = allocation;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("allocation", allocation)
				.append("action", action).toString();
	}
}