package com.example.connect.database.util;

public class StringUtils {

	public static int getJaroWinklerDistance(String s1, String s2) {
		return (int)(100 * org.apache.commons.lang3.StringUtils.getJaroWinklerDistance(
				org.apache.commons.lang3.StringUtils.defaultIfBlank(s1, ""),
				org.apache.commons.lang3.StringUtils.defaultIfBlank(s2, "")));
	}

}
